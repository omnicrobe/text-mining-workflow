This project uses snakemake to orchestrate the execution of the jobs. 
Singularity images of AlvisNLP are used to run the Alvis plans.
Make sure to have the prerequisites in the machine and then
clone and install the tools as described in the following sections.


### Prerequisities
These tools must be installed in the machine

* `GNU bash, version 4.4.20(1)-release (x86_64-pc-linux-gnu)`
* `git, version 2.17.1`
* `Snakemake, version 5.13.0`
* `singularity, version 3.5`
* `conda, version 4.8.3`
* `python, version 3.7.3`
* `maven, version 3.0.5`
* `openjdk, version 11.0.6`
* `perl 5, version 26`
    
### Clone the projet 

```bash
 git clone https://forgemia.inra.fr/omnicrobe/text-mining-workflow.git
 cd text-mining-workflow
 ```

### Install tools

The following steps allow to install the project tools

1. pull `Alvisnlp Singularity Image` into `text-mining-workflow/softwares`. AlvisnLP Singularity images are available into forgemia, your login/password are required. 
```
cd text-mining-workflow/softwares
singularity pull library://migale/default/alvisnlp:0.8.1
ls alvisnlp_0.8.1.sif 
```

2. clone [obo-utils](https://github.com/Bibliome/obo-utils) into text-mining-workflow/softwares
```
cd text-mining-workflow/softwares
git clone https://github.com/Bibliome/obo-utils.git
```

3. clone and install [alvisir](https://github.com/Bibliome/alvisir) into  text-mining-workflow/softwares

```
cd text-mining-workflow/softwares/
mkdir -p alvisir-install
git clone https://github.com/Bibliome/alvisir.git
cd alvisir/
mvn clean package
./install.sh ../alvisir-install
cd ..
rm -rf alvisir
```

4. create the global conda env for snakemake

```
cd text-mining-workflow/
conda env create -f softwares/envs/snakemake-5.13.0-env.yaml
conda activate snakemake-5.13.0-env
```

##  Set configs

adapt according to your installation, skip if you left default during the install.

1. set the path to `Alvisnlp Singularity Image` to the global variable `SINGULARITY_IMG:` of `config/config.yaml`
```
## alvisnlp singlarity image
SINGULARITY_IMG: "softwares/alvisnlp_0.8.1.sif"
```

2. set the path to `obo-utils` to the global variable `OBO_UTILS` of `config/config.yaml`
```
## obo-utils home
OBO_UTILS : "softwares/obo-utils"
```
3. set the path to `obo-utils` to the global variable `ALVISIR_HOME` of `config/config.yaml`

```
## alvisir home
ALVISIR_HOME : "softwares/alvisir-install"
```

> you may now run the pipelines as described [here](run.md)
