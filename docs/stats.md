

| SOURCE | LABEL | COUNT |
| -------- |-------- |-------- |
|  Pubmed | nb articles | count_files(corpora/pubmed/batches/*) x 1000  |
|  Pubmed | nb habitats | count_lines(corpora/pubmed/habitats.full.txt)  |      
|  Pubmed | nb relations | count_lines(corpora/pubmed/relations.full.txt)  |
|  pubmed | nb microorganisms | count_lines(corpora/pubmed/microorganisms.full.txt)  |   
|  Pubmed | nb uses | count_lines(corpora/pubmed/uses.full.txt)  |
|  Pubmed | nb phenotype-relations | count_lines(corpora/pubmed/phenotype-relations.full.txt)  |
|  Pubmed | nb uses-relations | count_lines(corpora/pubmed/uses-relations.full.txt)  |
|  Pubmed | nb phenotypes | count_lines(corpora/pubmed/phenotypes.full.txt)  |
|  cirm | nb entrees | count_lines(corpora/cirm/2019-07-05/extraction_3-fv.csv)  |
|  cirm | nb yeast entrees | count_lines(corpora/cirm/Levures_2017/data_CIRM_levures_extraction_09032017.csv)  |
|  cirm | nb entites | count_lines(corpora/cirm/mapped_taxids.txt)  |
|  cirm | nb yeast entities | count_lines(corpora/cirm/yeast_taxa.txt)  |
|  cirm | nb entites | count_lines(corpora/cirm/mapped_habitats.txt)  |
|  cirm | nb yeast habitats | count_lines(corpora/cirm/mapped_yeast_habitats.txt)  |
|  dsmz | nb entrees | count_lines(corpora/dsmz/dsmz-data/category=from_ncbi_taxonomy-key=taxid.tsv)  |
|  dsmz | nb entites | count_lines(corpora/dsmz/mapped_taxids.txt)  |
|  dsmz | nb habitats | count_lines(corpora/dsmz/mapped_habitats.txt)  |
|  genbank | nb entrees | count_lines(corpora/genbank/req1_sup800_bacteria-descriptors.csv)  |
|  genbank | nb entites | count_lines(corpora/genbank/mapped_taxids.txt)  |
|  genbank | nb habitats | count_lines(corpora/genbank/mapped_habitats.txt)  |