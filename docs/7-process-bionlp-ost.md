## About
This pipeline is the same as the PubMed processing one, using the same alvisnlp plan `plans/entities.plan`.
Rather than processing PubMed abstracts, it processes the test data from three tasks of the [BioNLP-OST 2019 Bacteria Biotope](https://sites.google.com/view/bb-2019/home), then evaluates the prediction with the API of the dedicated [online evaluation tool](http://bibliome.jouy.inra.fr/demo/BioNLP-OST-2019-Evaluation/index.html).

The three tasks are:
* `BB-norm+ner`: evaluates NER and normalization (Microorganism, Habitat and Phenotype).
* `BB-rel+ner`: evaluates NER and relation extraction (Lives_In and Exhibits).
* `BB-kb+ner`: evaluates knowledge base extraction performance.

The three datasets are available in `corpora/BioNLP-OST-2019/batches`. 

## Run the pipeline

```
snakemake --nolock --verbose --printshellcmds --use-singularity --use-conda --reason --latency-wait 30 --jobs 3 \
--snakefile evaluate-with-bionlp-ost.snakefile  \
--cluster "qsub -v PYTHONPATH=''  -V -cwd -e log/ -o log/ -q short.q -pe thread 2" \
--restart-times 4 all
```
## Display the DAG

```
snakemake --verbose \
--dag \
--printshellcmds \
--use-singularity \
--forceall \
--nolock \
--dry-run \
--reason \
--cores 4  \
--snakefile  evaluate-with-bionlp-ost.snakefile  \
all
| dot -Tsvg >  evaluate-with-bionlp-ost.snakefile.svg

show  evaluate-with-bionlp-ost.snakefile.svg
```

## **Resources used**

The pipeline relies on the following alvisnlp plan:
* `entities.plan`
	* `taxa.plan`
		* `strains-1.plan`
	* `syntax.plan`
		* `segmentation.plan`
			* `number-and-dates.plan`
	* `strains-2.plan`
	* `pos-tag-lemma-postprocessing.plan`
	* `tomap-habitats.plan`
		* `tag-food-derivative.plan`
		* `tag-food-process.plan`
	* `tomap-microbial-phenotypes.plan`
	* `use-extraction.plan`
	* `anaphora.plan`

The pipeline handles the following resources :
* inputs
    * `corpora/BioNLP-OST-2019/*/bionlp-st`
    * `share/OntoBiotope_BioNLP-OST-2019-Habitat.obo`
    * `share/OntoBiotope_BioNLP-OST-2019-Phenotype.obo`
    * `share/Use_V2.obo`
* outputs
	* `corpora/BioNLP-OST-2019/batch/*/eval.json`
* programs
    * `alvisnlp singularity container`
    * `python env`

<!-->
|solution |nb steps |
|--------|--------|
|AlvisNLP plans | xxx |
|bash scripts| xxx |
|python scripts | xxx |
| java jars | xxx |
