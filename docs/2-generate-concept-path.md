## About
The pipeline generates the concept paths from the structure of the Ontobiotope ontologies. 

 <img align="right" width="700" src="2-pipeline.svg">
<!--![generate concept paths](2-pipeline.svg)-->

## **Run the pipeline**

```
snakemake --nolock --verbose --printshellcmds --use-singularity --use-conda --reason --latency-wait 30 --jobs \
--snakefile generate_concept_path.snakefile \
--cluster "qsub -v PYTHONPATH=''  -V -cwd -e log/ -o log/ -q short.q -pe thread 2" \
--restart-times 4 all
```

## **Display the DAG**

```
snakemake --verbose \
--dag \
--printshellcmds \
--use-singularity \
--use-conda \
--forceall \
--nolock \
--reason \
--cores 4  \
--snakefile generate_concept_path.snakefile \
all \
| dot -Tsvg > generate_concept_path.snakefile.svg

show generate_concept_path.snakefile.svg
```

## Resources Used

The pipeline use the following alvisnlp plan :
* `plans/get_onto_paths.plan`

The pipeline handles the following resources :

* inputs
    * `share/BioNLP-OST+EnovFood-Habitat.obo`
    * `share/BioNLP-OST+EnovFood-Phenotype.obo`
    * `share/Use_V2.obo`
* outputs
    * `share/BioNLP-OST+EnovFood-Habitat.paths`
    * `share/BioNLP-OST+EnovFood-Phenotype.paths`
    * `share/Use_V2.paths`
* programs
    * `alvisnlp singularity container`

<!-->

|solution |nb steps |
|--------|--------|
|AlvisNLP plans | 3 |

