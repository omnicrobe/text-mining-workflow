## About
The pipeline extracts microorganisms, habitats of texts from DSMZ. The final result is stored into file `corpora/florilege/dsmz/dsmz-results.txt`

 <img align="right" width="100" src="5-pipeline.svg">
<!--![pipeline that processes and prepares data from dsmz](5-pipeline.svg)-->

## **Run the pipeline**

```
snakemake --nolock --verbose --printshellcmds --use-singularity --use-conda --reason --latency-wait 30 --jobs \
--snakefile process-dsmz.snakefile \
--cluster "qsub -v PYTHONPATH=''  -V -cwd -e log/ -o log/ -q short.q -pe thread 2" \
--restart-times 4 all
```

## **Display the DAG**

```
snakemake --verbose \
--dag \
--printshellcmds \
--use-singularity \
--forceall \
--nolock \
--dry-run \
--reason \
--cores 4  \
--snakefile  process-dsmz.snakefile  \
all \
| dot -Tsvg >  process-dsmz.snakefile.svg

show  process-dsmz.snakefile.svg
```

## **Resources used**

The pipeline relies on the following alvisnlp plan:
* `plans/taxa_generic.plan`
* `plans/map_habitats.plans`
	* `plans/syntax.plan`
		* `plans/segmentation.plan`
	* `plans/pos-tag-lemma-postprocessing.plan`
	* `plans/tomap-habitats-generic.plan`
		* `plans/tag-food-process.plan`
		* `plans/tag-food-derivative.plan`

The pipeline handles the following resources:
* inputs
    * `corpora/dsmz/dsmz-data/category=from_ncbi_taxonomy-key=taxid.tsv`
    * `corpora/dsmz/dsmz-data/category=origin-key=sample_type.tsv`
    * `share/OntoBiotope_BioNLP-OST-2019-Phenotype.obo` ?
* outputs
    * `corpora/florilege/dsmz/dsmz-results.txt`
* programs
    * `alvisnlp singularity container`
    * `python env`

<!-->
|solution |nb steps |
|--------|--------|
|AlvisNLP plans | 2 |
|bash scripts| 2 |
|python scripts | 1 |
