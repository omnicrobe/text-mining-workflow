## About
This pipeline analyzes the ontologies, cuts the desired branches and produces the tomap models and lexicon

 <img align="right" width="700" src="1-pipeline.svg">
<!--![ontology preprocessing](1-pipeline.svg)-->

## **Run the pipeline**

```
snakemake --nolock --verbose --printshellcmds --use-singularity --use-conda --reason --latency-wait 30 --jobs \
--snakefile preprocess-ontology.snakefile \
--cluster "qsub -v PYTHONPATH=''  -V -cwd -e log/ -o log/ -q short.q -pe thread 2" \
--restart-times 4 all
```

## **Display the DAG**

```
snakemake --verbose \
--dag \
--printshellcmds \
--use-singularity \
--use-conda \
--forceall \
--nolock \
--reason \
--cores 4  \
--snakefile preprocess-ontology.snakefile \
all \
| dot -Tsvg > preprocess-ontology.svg

show preprocess-ontology.svg
```


## **Resources used**
The pipeline relies on the following alvisnlp plans :
* `plans/biotope_ontology_analyzer.plan`
* `plans/phenotype_ontology_analyzer.plan`
* `plans/food_process_lex.plan`
* `plans/tag_ncbi_common_names.plan`

The pipeline handles the following resources :
* inputs
    * `share/BioNLP-OST+EnovFood.obo`
    * `share/ncbi-taxonomy/names.dmp`
* outputs
    * `share/BioNLP-OST+EnovFood-Habitat.json`
	* `share/BioNLP-OST+EnovFood-Phenotype.json`
	* `share/Use_V2.json`
	* `share/BioNLP-OST+EnovFood-Habitat.tomap`
	* `share/BioNLP-OST+EnovFood-Phenotype.tomap`
	* `share/food-process-lexicon.txt`
	* `share/NCBI_taxa_ontobiotope.txt`
* programs
    * `alvisnlp singularity container`
    * `conda env`


<!-->
|solution |nb steps |
|--------|--------|
|AlvisNLP plans | 4 |
|Python scripts | 5 |
|Bash scrips | 1 |


