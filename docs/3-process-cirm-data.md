## About
The pipeline extracts microorganisms, habitats from CIRM-BIA, CIRM-Levures and CIRM CFBP data sources.

 <img align="right" width="460" src="3-pipeline.svg">
<!--![pipeline that processes and prepares data from cirm](3-pipeline.svg)-->

## **Run the pipeline**

```
snakemake --nolock --verbose --printshellcmds --use-singularity --use-conda --reason --latency-wait 30 --jobs \
--snakefile process-cirm.snakefile \
--cluster "qsub -v PYTHONPATH=''  -V -cwd -e log/ -o log/ -q short.q -pe thread 2" \
--restart-times 4 all
```

## **Display the DAG**

```
snakemake --verbose \
--dag \
--printshellcmds \
--use-singularity \
--use-conda \
--forceall \
--nolock \
--reason \
--cores 4  \
--snakefile process-cirm.snakefile \
all \
| dot -Tsvg > process-cirm.snakefile.svg

show process-cirm.snakefile.svg
```

## **Resources**

The pipeline handles the following resources :
* inputs
    * `corpora/cirm/BIA_2021/florilege_export_final_17_02_21.xlsx` 
    * `corpora/cirm/Levures_2021/Florilege_21012021.xlsx`
    * `corpora/cirm/CFBP_2020/CFPB_22_sept_2020_Type.xlsx`
    * `share/OntoBiotope_BioNLP-OST-2019-Habitat.obo`
    * `share/OntoBiotope_BioNLP-OST-2019-Phenotype.obo` ?
* outputs
    * `corpora/florilege/cirm/cirm-bia-results.txt`
    * `corpora/florilege/cirm/cirm-yeast-results.txt`
    * `corpora/florilege/cirm/cirm-cfpb-results.txt`
* programs
    * `alvisnlp singularity container`
    * `python env`


The pipeline use the following alvisnlp plan:
* `plans/map_microorganisms.plan`
    * `plans/taxa_generic.plan`
        * `plans/strains-1.plan`
    * `plans/syntax.plan`
        * `plans/segmentation.plan`
    * `plans/strains-2.plan`
* `plans/map_habitats.plans`
	* `plans/syntax.plan`
		* `plans/segmentation.plan`
	* `plans/pos-tag-lemma-postprocessing.plan`
	* `plans/tomap-habitats-generic.plan`
		* `plans/tag-food-process.plan`
		* `plans/tag-food-derivative.plan`


<!-->
|solution |nb steps |
|--------|--------|
|AlvisNLP plans | 2 |
|bash scripts| 2 |
|python scripts | 1 |

