## About
The pipeline extracts microorganisms, habitats from the migale GenBank data (`/db/genbank`).
 <img align="right" width="300" src="4-pipeline.svg">
<!--![pipeline that processes and prepares data from cirm](4-pipeline.svg)-->

## **Run the pipeline**

```
snakemake --nolock --verbose --printshellcmds --use-singularity --use-conda --reason --latency-wait 30 --jobs \
--snakefile process-genbank.snakefile \
--cluster "qsub -v PYTHONPATH=''  -V -cwd -e log/ -o log/ -q short.q -pe thread 2" \
--restart-times 4 all
```

## **Display the DAG**

```
snakemake --verbose \
--dag \
--printshellcmds \
--use-singularity \
--forceall \
--nolock \
--dry-run \
--reason \
--cores 4  \
--snakefile process-genbank.snakefile \
all \
| dot -Tsvg > process-genbank.svg

show process-genbank.svg
```
## **Resources**

The pipeline handles the following resources :

* inputs
    * `/db/genbank/current/flat`
    * `share/ncbi-taxonomy-prefix/taxa+id_microorganisms.txt`
    * `share/OntoBiotope_BioNLP-OST-2019-Habitat.obo`
    * `share/OntoBiotope_BioNLP-OST-2019-Phenotype.obo` ?
* outputs
    * `corpora/florilege/genbank/genbank-results.txt`
* programs
    * `alvisnlp singularity container`
    * `python env`

The pipeline use the following alvisnlp plan:
* `plans/taxa_generic.plan`
* `plans/map_habitats.plans`
	* `plans/syntax.plan`
		* `plans/segmentation.plan`
	* `plans/pos-tag-lemma-postprocessing.plan`
	* `plans/tomap-habitats-generic.plan`
		* `plans/tag-food-process.plan`
		* `plans/tag-food-derivative.plan`




<!-->
|solution |nb actions |
|--------|--------|
|AlvisNLP plans | 2 |
|bash scripts| 2 |
|python scripts | 1 |
| perl scripts | 1 |

