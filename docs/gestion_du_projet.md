# Gestion du projet
 
Ce projet contient le workflow omnicrobe et la plupart des ressources utilisées pour mettre à jour la base de données de l'application Omnicrobe. Sa gestion est collaborative et implique la participation des membres pour maintenir et faire évoluer le code.
 
Le dépôt du projet admet deux branches permanentes, une *master* et une *dev*. Des branches temporaires sont créées à partir de la **dev** pour accueillir les nouveaux développements :
 
### Les branches du projet 
 
* La branche **dev** contient la version courante du workflow en développement. Elle sert à accueillir les développements validés (au moins opérationnellement)
* La branche  **master** contient une version du workflow stable et validée qualitativement à travers des mises à jour de la base Omnicrobe
* Les branches temporaires sont créées à partir des issues et ils sont liées à la branche **dev** comme décrits plus bas (Voir Section sur la modification du workflow)
 
### organisation du projet
 
Le projet contient plusieurs fichiers et dossiers importants. L'organisation du projet permet de faciliter la gestion de ces ressources :
 
* Les fichiers **`*.snakefile`** matérialisent les traitements autonomes du workflow sous forme de pipelines snakemake
* Le dossier **`plan/`** contient les plans d'exécution de AlvisNLP qui matérialisent les analyses spécifiques au text mining
* Le dossier **`corpora/`** doit contenir les données issues des différentes sources. Il doit également contenir les résultats finaux pour chaque source.
* Le dossier **`softwares/`** contient les environnements (conda), les scripts et les images (singularity) utilisés par les pipelines
* Le dossier **`share/`** contient les ressources additionnelles et certaines résultats intermédiaires des pipelines
* Le dossier **`config/`** contient les fichiers de configuration des pipelines. Ces fichiers donnent les valeurs aux paramètres et des options pour l'utilisation du cluster Migale
 
La suite de ce document décrit les procédures pour faire évoluer le workflow.
 
### Signaler des erreurs, problèmes, améliorations
 
Les erreurs et propositions d'amélioration à apporter sur le workflow ou sur les ressources du workflow doivent être décrites dans des `issues`.
 
Pour créer une nouvelle issue, vous pouvez aller sur la liste des [issues du projet](https://forgemia.inra.fr/omnicrobe/text-mining-workflow/-/issues) et utiliser le bouton `New issue`, puis renseigner un titre et une description du probléme. Vous avez la possibilité de joindre des fichiers et d'ajouter des liens externes.
 
### Modifier le code ou un fichier du workflow
 
Les modifications ou ajouts sur les ressources du workflow se font selon une collaboration entre les **développeurs** qui implémentent et soumettent les modifications et les **administrateurs** qui valident et intègrent les modifications. Cette collaboration est importante pour une évolution maîtrisée et cohérente.
 
#### Coté développeurs
 
Les développeurs doivent apporter les modifications aux ressources du projet en tenant compte de l'organisation du projet (voir Section *organisation projet* en haut. Les modifications doivent respecter les étapes suivantes :
 
1. Identifier  (à défaut créer) une issue pour le problème à résoudre
 
Pour identifier une issue, afficher la liste des issues qui est disponible [ici](https://forgemia.inra.fr/omnicrobe/text-mining-workflow/-/issues). Pour créer une issue, voir la Section ** Signaler des erreurs, problèmes, améliorations**.
 
 
2. Créer une branche à partir de la branche de développement **dev**
 
La branche **dev** reçoit toutes les modifications à apporter sur le projet. Pour créer une nouvelle branche, afficher la issue concernée et utiliser le bouton créer une branche (choisir ensuite l'option `créer une branche` et mettre la branche **dev** dans la case `source`). Le nom de la branche est proposé automatiquement (à partir du nom de l’issue).
 
Pour créer une branche à partir de la ligne de commande, afficher les détails du help suivant `$ git help branch`
 
3. Effectuer les modifications sur la branche créée
 
Il faut effectuer les modifications sur la branche créée à l'étape précédente en utilisant l'interface GitLab ou en se positionnant via la ligne de commande comme suit.
 
```
git pull
git checkout [NOM_LA_BRANCHE]
```
 
Les `commit/push` effectuées après modification sur la branche vont déclencher une évaluation automatique du workflow sur le corpus [bactéria biotope](https://sites.google.com/view/bb-2019/home) et les résultats seront disponibles dans le fichier nommé "current_BB_eval_scores.txt". (**pas encore implémenté**)
 
4. Demander un `merge request`
 
Après les développements, effectuer une requête d'intégration (**Merge request**) de ses modifications qui est reçue et traité par les administrateurs (voir **Côté Administrateur**)
 
Pour envoyer une **Merge request**, aller sous *Repository > Branches*, puis identifier la branche contenant les modifications et envoyer la requête en utilisant le bouton *Merge request*.
 
Toujours s'assurer que le workflow fonctionne dans la branche temporaire et que les résultats obtenus sont ceux attendus avant de faire le `merge request`.
 
 
#### Coté administrateurs
 
Les administrateurs doivent assurer l'intégration et la validation des développements envoyés par les développeurs. Ils traitent ainsi les requêtes d'intégration.
 
1. Vérifier les résultats de l'évaluation automatique sur la branche temporaire de développement
 
Après chaque *merge/push*, le workflow est évalué automatiquement sur les données de bacteria Biotope. Cela permet de s'assurer que les modifications proposées ne comportent pas de bugs et qu'elles ne dégradent pas la qualité des résultats. (**pas encore implémentée**)
 
Pour vérifier les résultats de l'évaluation sur les modifications, il faut afficher la [liste des branches](https://forgemia.inra.fr/omnicrobe/text-mining-workflow/-/branches) et vérifier sous la branche contenenant les modifications le fichier nommé "current_BB_eval_scores.txt". (**pas encore implémenté**)
 
2. Faire le merge
 
L'intégration d'une branche contenant des modifications ne doit être effectuée que si les résultats de la branche en question sont validés (à faire par l’administrateur). L'intégration consiste ensuite à ajouter les modifications dans la branche **dev** en ouvrant le **merge resquest** envoyé à partir de la [liste des merge requests](https://forgemia.inra.fr/omnicrobe/text-mining-workflow/-/merge_requests).
 
L'intégration peut impliquer des conflits et des erreurs sous la branche **dev**. Il faudra faire toutes les corrections pour s'assurer que les modifications s'intègrent parfaitement. Cette étape peut nécessiter de revenir vers les développeurs et de créer de nouvelles issues.
 
4. Vérifier les résultats de l'évaluation automatiquement sur la branche développement
 
Comme pour les branches temporaires, après chaque `merge/push` le workflow est évalué automatiquement sur les données de bacteria Biotope. Il faudra vérifier les résultats du workflow sous la branche **dev** après chaque intégration de nouvelles modifications.
 
Cette étape peut impliquer les développeurs si la qualité des données se dégrade ou si des erreurs apparaissent.
 
## Mise à jour de la base Omnicrobe
 
Le workflow est utilisé pour mettre à jour la base de données omnicrobe. Il s'agit d'une opération plus ou moins lourde qui impliquent plusieurs acteurs et plusieurs étapes :
 
 
ETAPE 1. développements et intégration des nouvelles modifications sur la branche **dev**
 
ETAPE 2. expérience de mise à jour qui peut impliquer des corrections et de revenir à l'étape 1
 
ETAPE 3. validation de la qualité des résultats avec une validation sur Bacteria Biotope et une validation visuelle via [AlvisIR DEV](https://bibliome.migale.inrae.fr/omnicrobe-dev/alvisir/webapi/search) et [Omnicrobe DEV](omnicrobe-dev.migale.inrae.fr/) par des utilisateurs
 
ETAPE 4. effectuer l’intégration finale vers la base omnicrobe Prod si les étapes précédentes passent. L'intégration peut être annulée ou reprogrammée si les résultats après l'étape de validation ne sont pas satisfaisantes.
 
 
### Quelques autres informations utiles (ajouter les points non traités ici)
 
Point
 
 

