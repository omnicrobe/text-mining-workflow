## Create and activate the snakemake env
> the workflow requires snakemake 5.13 to run. We provide a conda environment of snakemake 
to create and activate as following.
```
cd  text-mining-workflow/
conda env create -f softwares/envs/snakemake-5.13.0-env.yaml
conda activate snakemake-5.13.0-env
```
## Ontology

* Ontobiotope is required in all steps, it must be available in folder `ancillaries` and its path set into the [config.yaml](config/config.yaml)

## Corprora

* CIRM corpus is processed in **step 3.**, the corpus and results are localized into the `corpora/cirm` folder.
* GenBank corpus is processed in **step 4.**, the corpus and results are localized into the `corpora/genbank` folder. 
* DSMZ corpus is processed in **step 5.**, the corpus and results are localized into the `corpora/dsmz` folder.
* Pubmed corpus is processed in **step 6.**, the corpus and results are localized into the `corpora/pubmed` folder. 
Pubmed corpus is to split into several batches to put in the `corpora/pubmed/batches` folder. 

## NCBI Taxonomy

NCBI taxonomy is required. The NCBI resources are generated using [BacDive Utils](https://forgemia.inra.fr/florilege/bacdive-utils) 
* NCBI taxa and ids (extended) : `share/supertaxo/taxa+id_full.txt`
* NCBI taxa ids and synonyms : `share/supertaxo/taxid_microorganisms.txt`
* NCBI taxa : `share/supertaxo/names.dmp`
* NCBI-Bacdive Match : `share/supertaxo/bacdive-to-taxid.txt`

## Expander

* The expander config file (`share/expander.xml`) is required to create the index expander for AlvisIR in **step 6.**
Edit file `share/expander.xml` to add correct paths

## Run the pipelines
The commands to run the steps look like the following. They must be executed from the project home dir with the `snakemake-5.13.0-env` activated.  
```
snakemake --nolock --verbose --printshellcmds --use-singularity --use-conda --reason --latency-wait 10 --jobs \
--snakefile [STEP].snakefile \
--cluster "qsub -v PYTHONPATH=''  -V -cwd -e log/ -o log/ -q short.q -pe thread 2" \
--restart-times 4 all
--dry-run
```
* `[STEP]` to be replaced by `preprocess-ontology` or `generate_concept_path` or `process_CIRM_corpus` or `process_GenBank_corpus` or `process_DSMZ_corpus` or `process_PubMed_corpus`
* use option `--dry-run` to display what would be done without executing
* use option `-nolock` to avoid locking execution files.
* use option `--use-singularity` and `--use-conda` to manage the singularity images and the conda environments.
* use option `--reason` to print the reason for each executed rule.
* use option `--latency-wait` to set the number of seconds to wait for an output file. 
* Use option `--jobs` to set the number of jobs to tun in parallel. 
* use option `--cluster` to configure the SGE cluster. It is not used when you run locally. 

These others options can be useful
* use option `--forceall` if you want to force the execution of all the rules
* use option `--delete-all-output` to remove the already calculated outputs
* use option `--unlock` to unlock files
* use option `--directory` to set the execution directory
* use option `--report` to create an HTML report with results and statistics

More options for snakemake [here](https://snakemake.readthedocs.io/en/v5.13.0/api_reference/snakemake.html)

The following commands for each step run on the Migale cluster

### **step 1.** `preprocess Ontology` <!--to analyze the ontologies, cut the desired branches and produce the tomap models and lexicon to be used in the next steps. -->
   
```
snakemake --nolock --verbose --printshellcmds --use-singularity --use-conda --reason --latency-wait 30 --jobs \
--snakefile preprocess-ontology.snakefile \
--cluster "qsub -v PYTHONPATH=''  -V -cwd -e log/ -o log/ -q short.q -pe thread 2" \
--restart-times 4 all
```

### **step 2.** `process CIRM data` <!--to extract microorganisms, habitats of texts from CIRM. -->

```
snakemake --nolock --verbose --printshellcmds --use-singularity --use-conda --reason --latency-wait 30 --jobs \
--snakefile process_CIRM_corpus.snakefile \
--cluster "qsub -v PYTHONPATH=''  -V -cwd -e log/ -o log/ -q short.q -pe thread 2" \
--restart-times 4 all
```

### **step 3.** `process GenBank data` <!--to extract microorganisms, habitats of texts from GenBank.-->

```
snakemake --nolock --verbose --printshellcmds --use-singularity --use-conda --reason --latency-wait 30 --jobs \
--snakefile process_GenBank_corpus.snakefile \
--cluster "qsub -v PYTHONPATH=''  -V -cwd -e log/ -o log/ -q short.q -pe thread 2" \
--restart-times 4 all
```

### **step 4.** `process DSMZ data` <!--to extract microorganisms, habitats of texts from DSMZ. -->

```
snakemake --nolock --verbose --printshellcmds --use-singularity --use-conda --reason --latency-wait 30 --jobs \
--snakefile process_DSMZ_corpus.snakefile \
--cluster "qsub -v PYTHONPATH=''  -V -cwd -e log/ -o log/ -q short.q -pe thread 2" \
--restart-times 4 all
```

### **step 5.** `process Pubmed Data` <!--to extracts microorganisms, habitats of texts from Pubmed. -->

```
snakemake --nolock --verbose --printshellcmds --use-singularity --use-conda --reason --latency-wait 30 --jobs 80 \
--snakefile process_PubMed_corpus.snakefile \
--cluster "qsub -v PYTHONPATH=''  -V -cwd -e log/ -o log/ -q short.q -pe thread 2" \
--restart-times 4 all
```

### **all steps.** `run all steps at once`
A report is provided at the end.
```
snakemake --verbose --printshellcmds --use-singularity --nolock --reason --latency-wait 30  --jobs 100 \
--snakefile all.snakefile all
```
> `all steps.` is executed in local, option `--cluster` is not used since already used in the others steps.

Generate a report.
```
snakemake --verbose --printshellcmds --use-singularity --nolock --reason --latency-wait 30  --jobs 100 \
--snakefile all.snakefile all \
--report report.html
```
