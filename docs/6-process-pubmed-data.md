## About
The pipeline extracts microorganisms, habitats of texts from Pubmed. The workflow uses alvisnlp plan `plans/entities.plan`. 
Pubmed corpus is split into several batches that are available into `corpora/pubmed/batches`. 
The bacthes are automatically scanned by the pipeline.
 
 <img align="right" width="800" src="6-pipeline.svg">
 <!--[Running the pipeline `entities.plan` on the batches](6-pipeline.svg)-->


## **Run the pipeline**

```
snakemake --nolock --verbose --printshellcmds --use-singularity --use-conda --reason --latency-wait 30 --jobs 80 \
--snakefile process-pubmed.snakefile \
--cluster "qsub -v PYTHONPATH=''  -V -cwd -e log/ -o log/ -q short.q -pe thread 2" \
--restart-times 4 all
```


## **Display the DAG**

```
snakemake --verbose \
--dag \
--printshellcmds \
--use-singularity \
--forceall \
--nolock \
--dry-run \
--reason \
--cores 4  \
--snakefile  process-pubmed.snakefile \
all
| dot -Tsvg >  process-pubmed.snakefile.svg

show  process-pubmed.snakefile.svg
```

## **Resources used**

The pipeline relies on the following alvisnlp plan:
* `entities.plan`
	* `taxa.plan`
		* `strains-1.plan`
	* `syntax.plan`
		* `segmentation.plan`
			* `number-and-dates.plan`
	* `strains-2.plan`
	* `pos-tag-lemma-postprocessing.plan`
	* `tomap-habitats.plan`
		* `tag-food-derivative.plan`
		* `tag-food-process.plan`
	* `tomap-microbial-phenotypes.plan`
	* `use-extraction.plan`
	* `anaphora.plan`

The pipeline handles the following resources :
* inputs
    * `corpora/pubmed`
    * `share/OntoBiotope_BioNLP-OST-2019-Habitat.obo`
    * `share/OntoBiotope_BioNLP-OST-2019-Phenotype.obo`
    * `share/Use_V2.obo`
	* `ncbi-taxonomy-prefix/taxa+id_full.txt`
	* `ncbi-taxonomy-prefix/taxid_microorganisms.txt`
	* `ncbi-taxonomy-prefix/names.dmp`
* outputs
	* `corpora/pubmed/expander`
	* `corpora/pubmed/index`
	* `corpora/florilege/pubmed/PubMed-*.txt`
* programs
    * `alvisnlp singularity container`
    * `python env`


<!-- >
|solution |nb steps |
|--------|--------|
|AlvisNLP plans | 1 |
|bash scripts| 8 |
|python scripts | 2 |
| java jars | 2 |
