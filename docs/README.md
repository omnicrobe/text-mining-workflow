# About
This projet contains a workflow designed to extract microorganism taxa, their habitats and their phenotypes 
from texts and categorize the extracted information by means of taxa from the NCBI taxonomy and concepts 
from the OntoBiotope ontology. It uses texts downloaded from CIRMS, GenBank, DSMZ and PubMed data Banks. 

The workflow is composed of six pipelines which represent text mining processes relying on [AlvisNLP plans](https://bibliome.github.io/alvisnlp/) and some additional tools. 
It is designed to feed the [Omnicrobe database](https://omnicrobe.migale.inrae.fr/) with rich text mining knowledges related to food microbe flora. 

We use [Snakemake](https://snakemake.readthedocs.io) pipelines to materialize the execution **steps**: 

<img align="right" width="460" height="300" src="docs/pipeline.svg">

1. **`Preprocess ontology`** to analyze the ontologies, cut the desired branches, produce the tomap models and lexicon, and create the concept paths from the structure of the ontologies. These resources are used in the next steps. 
[details here...](1-preprocess-ontology.md)
<!-- what contains the files *.tomap -->
<!-- what is the *.json ontology format -->
<!-- quelles sont les ontologies BioNLP-OST+EnovFood*.obo -->

2. **`Process CIRM corpus`** to extract microorganisms, habitats from CIRM data. 
[details here...](3-process-cirm-data.md)

3. **`Process GenBank corpus`** to extract microorganisms, habitats from GenBank data. 
[details here...](4-process-genbank-data.md)

4. **`Process DSMZ corpus`** to extract microorganisms, habitats from DSMZ data. 
[details here...](5-process-dsmz-data.md)

5. **`Process PubMed corpus`** to extract microorganisms, habitats from Pubmed abstracts. 
[details here...](6-process-pubmed-data.md)

6. **`Process BioNLP-OST 2019 test corpus`** to evaluate the results on a reference dataset. 
[details here...](7-process-bionlp-ost.md)


The workflow relies on the following `structure of folders` to manage the resources:
```
    ├── README.md
    ├── LICENSE.md
    ├── config
    │   ├── config.yaml
    │   └── resources.yaml
    ├── *-snakefile
    ├── plans
    │   └── *.plan
    ├── corpora
    │   ├── cirm/
    │   ├── genbank/
    │   ├── dsmz/
    │   └── pubmed/
    ├── ancillaries
    │    ├── *.obo
    │    ├── *.txt
    │    └── *.tomap
    └── softwares
         ├── *.env
         ├── *.simg
         ├── *.python
         ├── *.perl
         ├── *.bash
         └── *.jar
```

The main directories/files are:
* `plans/` contains the AlvisNLP plans that are text mining pipelines to extract the informations 
* `corpora/` contains the textual data to process
* `share/` contains ancillary data like ontologies, lexical data, grammars, language models
* `softwares/` contains additional scripts, envs and config files
* `*.snakefile` are the snakemake pipelines which represent the execution entry points


## [How to install ?](docs/install.md)
## [How to run ?](docs/run.md)


<!--
## Sub workflows

### Ontology preprocessing 
this pipeline is run when there is a new of version of the ontobiotope ontology

![ontology preprocessing](biotopes/pipeline.svg)
    
### Generate concept path
this pipeline generate the concept paths for SD

![generate concept paths](biotopes/pipeline2.svg)

### Processing data from bank
| Process CIRM data | Process GenBank data | Process DSMZ data | Process Pubmed Data |
| ------------- | ------------- | ------------- | ---------------- |
|![pipeline that processes and prepares data from cirm]( biotopes/pipeline3.1.svg ) | ![pipeline that processes and prepares data from genbank]( biotopes/pipeline3.2.svg ) | ![pipeline that processes and prepares data from dsmz]( biotopes/pipeline3.3.svg ) | ![Running the pipeline `entities.plan` on the batches](biotopes/pipeline4.svg) |

### Running the pipeline `entities.plan` on the batches of microbes-2019 corpus

![Running the pipeline `entities.plan` on the batches](biotopes/pipeline4.svg)

## Automate the way Florilege access to the text mining data

1. Inventory of the data and programs used in the alvis plan to produce the data

2. Define snakemake pipelines to integrate the alvis plan used to generate the data


## Notes
See NOTES into projet-work/biotopes/Florilege/

## Tools

* GNU bash, version 4.4.20(1)-release (x86_64-pc-linux-gnu)
* snakemake version 5.13.0
* singularity version 3.5 
* conda version 4.8.3
* python version 3.7.3
* maven version 3.0.5
* openjdk version 11.0.6

## Usual commands

`snakemake --verbose --printshellcmds --use-singularity --forceall --nolock --reason --dry-run --snakefile Snakefile-4`

`snakemake --dag --verbose --printshellcmds --use-singularity --forceall --nolock --reason --dry-run --snakefile Snakefile-4 | dot -Tsvg > pipeline3.2.svg`

`snakemake --verbose --printshellcmds --use-singularity --forceall --nolock --reason  --snakefile Snakefile-4`

## Languages
* Java
* Python
* Bash
* Make
-->
