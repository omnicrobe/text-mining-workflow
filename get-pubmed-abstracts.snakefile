## config file
configfile: "config/config.yaml"


''' 
all
'''
rule all:
    input:
        corpus="corpora/pubmed/batches",
        #pmcid_file = "corpora/epmc/pmids",
        meta = "corpora/pubmed/info.csv"
		

'''
generate microbio corpus from the pubmed databank
'''
rule get_abstracts:
    input:
        db=config["PUBMED_DB"]
    output:
        corpus=directory("corpora/pubmed/batches")
    params:
        request="corpora/pubmed/microbio-mesh-terms.txt"
    conda: "softwares/envs/pubmed-index-env.yaml"
    shell: """
    softwares/java-utils/bin/pubmed-search -index {input.db} -outdir {output.corpus} -xml %%/batch.xml -mesh-tree-query {params.request} -batch 1000
        """


'''
generate microbio pmids from the pubmed databank
'''
rule get_pmids:
    input:
        db=config["PUBMED_DB"]
    output:
        corpus=directory("corpora/pubmed/pmids")
    params:
        request="corpora/pubmed/microbio-mesh-terms.txt"
    conda: "softwares/envs/pubmed-index-env.yaml"
    shell: """
    softwares/java-utils/bin/pubmed-search -index {input.db} -outdir {output.corpus} -pmid %%/pmids.txt -mesh-tree-query {params.request} -batch 100
        """


'''
get metadata
'''
rule get_meta:
	input:
		corpora="corpora/pubmed/batches"
	output:
		meta="corpora/pubmed/info.csv"
	run:
		import datetime
		import pandas
		date = datetime.datetime.now().strftime("%Y%m%d")
		batches, = glob_wildcards(input.corpora + "/{id}/batch.xml")
		corpus_size = len(batches)
		data = {"source": "PubMed", 
			"name": ["microbio"], 
			"size (nb. batches x 1000 abstracts)" : [corpus_size], 
			"date" : [date]}
		df = pandas.DataFrame(data)
		df.to_csv(output.meta, index=False)
