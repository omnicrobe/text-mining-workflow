## Processing data sources CIRM

## config file
configfile: "config/config.yaml"

'''
## variables, check values into config file
_ontobiotope = config['ONTOBIOTOPE']
_names = config['NCBI_TAXO_NAMES']
_pubmed_batches_home = config['PUBMED_BATCHES_HOME']
_pubmed_xslt_file = config['PUBMED_XSLT_FILE']
_ncbi_taxo_microorganisms = config['NCBI_TAXO_MICROORGANISMS']
_ncbi_taxo_id = config['NCBI_TAXO_ID']
_ncbi_taxo_and_id_microorganisms = config['NCBI_TAXO_AND_ID_MICROORGANISMS']
_genbank_db = config['GENBANK_DB']
_dsmz_habitat_corpus = config['DSMZ_HABITAT_CORPUS']
_map_bacdive_taxid = config['MAP_BACDIVE_TAXID']
_cirm_bia_corpus = config['CIRM_BIA_CORPUS']
_cirm_yeast_corpus = config['CIRM_YEAST_CORPUS']
_cirm_cfbp_corpus = config['CIRM_CFBP_CORPUS']
'''

'''
all
'''
rule all:
	input:
		'corpora/florilege/cirm/cirm-bia-results.txt',
		'corpora/florilege/cirm/cirm-yeast-results.txt',
		'corpora/florilege/cirm/cirm-cfbp-results.txt'

'''
get taxa and habitats (CIRM BIA)
'''
rule get_cirm_bia_taxa_habitats:
	input:
		file=config['CIRM_BIA_CORPUS']
	params:
		taxa_index='2',
		strain_index='1',
		habitat_index='15'
	output:
		taxa='corpora/cirm/bia/bia_taxa.txt',
		habitats='corpora/cirm/bia/bia_habitats.txt',
		tsv='corpora/cirm/BIA_final/florilege_export_final.tsv'
	conda: 'softwares/envs/python3_pandas_env.yaml'
	shell: """
		python3.7 softwares/scripts/preprocess-cirm.py --input {input.file} --taxa-index {params.taxa_index} --habitat-index {params.habitat_index} --strain-index {params.strain_index} --taxa-outfile {output.taxa} --habitat-outfile {output.habitats} --tsv-outfile {output.tsv}
	      """

'''
get taxa and habitats (CIRM Levures)
'''
rule get_cirm_yeast_taxa_habitats:
	input:
		file=config['CIRM_YEAST_CORPUS']
	params:
		taxa_index='1',
		habitat_index='10,11'
	output:
		taxa='corpora/cirm/levures/yeast_taxa.txt',
		habitats='corpora/cirm/levures/yeast_habitats.txt',
		tsv='corpora/cirm/Levures_final/florilege_export_final.tsv'
	conda: 'softwares/envs/python3_pandas_env.yaml'
	shell: """
		python3.7 softwares/scripts/preprocess-cirm-yeast.py --input {input.file} --taxa-index {params.taxa_index} --habitat-index {params.habitat_index} --taxa-outfile {output.taxa} --habitat-outfile {output.habitats} --tsv-outfile {output.tsv}
	      """

'''
get taxa and habitats (CIRM CFBP)
'''
rule get_cirm_cfbp_taxa_habitats:
	input:
		file=config['CIRM_CFBP_CORPUS']
	params:
		taxa_index='2',
		strain_index='0',
		habitat_index='11,30,31,32,46'
	output:
		taxa='corpora/cirm/cfbp/cfbp_taxa.txt',
		habitats='corpora/cirm/cfbp/cfbp_habitats.txt',
		tsv='corpora/cirm/CFBP_final/florilege_export_final.tsv'
	conda: 'softwares/envs/python3_pandas_env.yaml'
	shell: """
		python3.7 softwares/scripts/preprocess-cirm-cfbp.py --input {input.file} --taxa-index {params.taxa_index} --strain-index {params.strain_index} --habitat-index {params.habitat_index} --taxa-outfile {output.taxa} --habitat-outfile {output.habitats} --tsv-outfile {output.tsv}
	      """


'''
map microorganisms
'''
rule map_cirm_bia_microorganisms:
	input:
		taxa='corpora/cirm/bia/bia_taxa.txt'
	output:
		mapped_taxaids='corpora/cirm/bia/mapped_bia_taxa.txt'
	params:
		plan='plans/map_microorganisms.plan',
		taxid_microorganisms=config['NCBI_TAXO_MICROORGANISMS'],
		taxa_id_full=config['NCBI_TAXO_ID']
	singularity:config["SINGULARITY_IMG"]
	shell: """alvisnlp -J-Xmx32g -cleanTmp -verbose \
	-alias input {input.taxa} \
	-alias output {output.mapped_taxaids} \
	-alias taxid_microorganisms {params.taxid_microorganisms} \
	-alias taxa+id_full {params.taxa_id_full} \
	{params.plan}
	"""

'''
map microorganisms (CIRM Levures)
'''
rule map_cirm_yeast_microorganisms:
	input:
		taxa='corpora/cirm/levures/yeast_taxa.txt'
	output:
		mapped_taxaids='corpora/cirm/levures/mapped_yeast_taxa.txt'
	params:
		plan='plans/map_microorganisms.plan',
		taxid_microorganisms=config['NCBI_TAXO_MICROORGANISMS'],
		taxa_id_full=config['NCBI_TAXO_ID']
	singularity:config["SINGULARITY_IMG"]
	shell: """alvisnlp -J-Xmx32g -cleanTmp -verbose \
	-alias input {input.taxa} \
	-alias output {output.mapped_taxaids} \
	-alias taxid_microorganisms {params.taxid_microorganisms} \
	-alias taxa+id_full {params.taxa_id_full} \
	{params.plan}
	"""


'''
map microorganisms (CIRM CFBP)
'''
rule map_cirm_cfbp_microorganisms:
	input:
		taxa='corpora/cirm/cfbp/cfbp_taxa.txt'
	output:
		mapped_taxa='corpora/cirm/cfbp/mapped_cfbp_taxa.txt'
	params:
		plan='plans/map_microorganisms.plan',
		taxid_microorganisms=config['NCBI_TAXO_MICROORGANISMS'],
		taxa_id_full=config['NCBI_TAXO_ID']
	singularity:config["SINGULARITY_IMG"]
	shell: """alvisnlp -J-Xmx32g -cleanTmp -verbose \
	-alias input {input.taxa} \
	-alias output {output.mapped_taxa} \
	-alias taxid_microorganisms {params.taxid_microorganisms} \
	-alias taxa+id_full {params.taxa_id_full} \
	{params.plan}
	"""

'''
map habitats of microorganisms
'''
rule map_cirm_habitats:
	input:
		habitats='corpora/cirm/bia/bia_habitats.txt'
	output:
		mapped_habitats='corpora/cirm/bia/mapped_bia_habitats.txt',
		yatea_candidates='corpora/cirm/bia/yatea/candidates.xml',
                yatea_var_candidates='corpora/cirm/bia/yatea-var/candidates.xml'
	params:
		plan='plans/map_habitats.plan',
		onto='share/BioNLP-OST+EnovFood-Habitat.obo',
		tomap='share/BioNLP-OST+EnovFood-Habitat.tomap',
		graylist='share/graylist_extended.heads',
		emptywords='share/stopwords_EN.ttg',
		outdir='corpora/cirm/bia',
		outfile='mapped_bia_habitats.txt'
	singularity:config["SINGULARITY_IMG"]
	shell: """alvisnlp -J-Xmx32g -cleanTmp -verbose \
	-alias input {input.habitats} \
    -outputDir {params.outdir} \
	-alias output {params.outfile} \
	-alias ontobiotope {params.onto} \
	-xalias '<ontobiotope-tomap empty-words="{params.emptywords}" graylist="{params.graylist}" whole-proxy-distance="false">{params.tomap}</ontobiotope-tomap>' \
	{params.plan}
	"""
	
'''
map habitats of microorganisms (CIRM Levures)
'''
rule map_cirm_yeast_habitats:
	input:
		habitats='corpora/cirm/levures/yeast_habitats.txt'
	output:
		mapped_habitats='corpora/cirm/levures/mapped_yeast_habitats.txt',
                yatea_candidates='corpora/cirm/levures/yatea/candidates.xml',
                yatea_var_candidates='corpora/cirm/levures/yatea-var/candidates.xml'
	params:
		plan='plans/map_habitats.plan',
		onto='share/BioNLP-OST+EnovFood-Habitat.obo',
		tomap='share/BioNLP-OST+EnovFood-Habitat.tomap',
		graylist='share/graylist_extended.heads',
		emptywords='share/stopwords_EN.ttg',
		outdir='corpora/cirm/levures',
		outfile='mapped_yeast_habitats.txt'
	singularity:config["SINGULARITY_IMG"]
	shell: """alvisnlp -J-Xmx32g -cleanTmp -verbose \
	-alias input {input.habitats} \
    -outputDir {params.outdir} \
	-alias output {params.outfile} \
	-alias ontobiotope {params.onto} \
	-xalias '<ontobiotope-tomap empty-words="{params.emptywords}" graylist="{params.graylist}" whole-proxy-distance="false">{params.tomap}</ontobiotope-tomap>' \
	{params.plan}
	"""

'''
map habitats of microorganisms (CIRM CFBP)
'''
rule map_cirm_cfbp_habitats:
	input:
		habitats='corpora/cirm/cfbp/cfbp_habitats.txt'
	output:
		mapped_habitats='corpora/cirm/cfbp/mapped_cfbp_habitats.txt',
                yatea_candidates='corpora/cirm/cfbp/yatea/candidates.xml',
                yatea_var_candidates='corpora/cirm/cfbp/yatea-var/candidates.xml'
	params:
		plan='plans/map_habitats.plan',
		onto='share/BioNLP-OST+EnovFood-Habitat.obo',
		tomap='share/BioNLP-OST+EnovFood-Habitat.tomap',
		graylist='share/graylist_extended.heads',
		emptywords='share/stopwords_EN.ttg',
		outdir='corpora/cirm/cfbp',
		outfile='mapped_cfbp_habitats.txt'
	singularity:config["SINGULARITY_IMG"]
	shell: """alvisnlp -J-Xmx32g -cleanTmp -verbose \
	-alias input {input.habitats} \
    -outputDir {params.outdir} \
	-alias output {params.outfile} \
	-alias ontobiotope {params.onto} \
	-xalias '<ontobiotope-tomap empty-words="{params.emptywords}" graylist="{params.graylist}" whole-proxy-distance="false">{params.tomap}</ontobiotope-tomap>' \
	{params.plan}
	"""

'''
format results
'''
rule format_cirm_results:
	input:
		file='corpora/cirm/BIA_final/florilege_export_final.tsv',
		taxa='corpora/cirm/bia/mapped_bia_taxa.txt',
		habitats='corpora/cirm/bia/mapped_bia_habitats.txt'
	output:
		result='corpora/florilege/cirm/cirm-bia-results.txt'
	params:
		taxa_index='2',
		strain_index='1',
		habitat_index='15'
	conda: 'softwares/envs/obo-utils-env.yaml'
	shell: 'python softwares/scripts/format-cirm-results.py --cirm {input.file} --taxa {input.taxa} --habitats {input.habitats} --taxa-index {params.taxa_index} --strain-index {params.strain_index} --habitat-index {params.habitat_index} > {output.result}'

'''
format results (CIRM Levures)
'''
rule format_cirm_yeast_results:
	input:
		file='corpora/cirm/Levures_final/florilege_export_final.tsv',
		taxa='corpora/cirm/levures/mapped_yeast_taxa.txt',
		habitats='corpora/cirm/levures/mapped_yeast_habitats.txt'
	output:
		result='corpora/florilege/cirm/cirm-yeast-results.txt'
	params:
		taxa_index='1',
		habitat_index='10,11'
	conda: 'softwares/envs/obo-utils-env.yaml'
	shell: 'python softwares/scripts/format-cirm-yeast-results.py --cirm {input.file} --taxa {input.taxa} --habitats {input.habitats} --taxa-index {params.taxa_index} --habitat-index {params.habitat_index} > {output.result}'

'''
format results (CIRM CFBP)
'''
rule format_cirm_cfbp_results:
	input:
		file='corpora/cirm/CFBP_final/florilege_export_final.tsv',
		taxa='corpora/cirm/cfbp/mapped_cfbp_taxa.txt',
		habitats='corpora/cirm/cfbp/mapped_cfbp_habitats.txt'
	output:
		result='corpora/florilege/cirm/cirm-cfbp-results.txt'
	params:
		taxa_index='2',
		strain_index='0',
		habitat_index='11,30,31,32,46'
	conda: 'softwares/envs/obo-utils-env.yaml'
	shell: 'python softwares/scripts/format-cirm-cfbp-results.py --cirm {input.file} --taxa {input.taxa} --habitats {input.habitats} --taxa-index {params.taxa_index} --strain-index {params.strain_index} --habitat-index {params.habitat_index} > {output.result}'



'''---------------------------THIS PART TO GET META INFO-------------------------'''

'''
cirm | nb entrees bia | count_lines(corpora/cirm/BIA_2022/Omnicrobe_03-2022.xlsx)
cirm | nb entrees cfbp | count_lines(corpora/cirm/CFBP_2022/ExportBaseCIRM-CFBP_Omnicrobe_8mars2022.xlsx)
cirm | nb entrees levures | count_lines(corpora/cirm/Levures_2021/Florilege_23082021.xlsx)
'''
ENTREES_CIRM = ["BIA_2022/Omnicrobe_03-2022.xlsx", "corpora/cirm/Levures_2021/Florilege_23082021.xlsx", "CFBP_2022/ExportBaseCIRM-CFBP_Omnicrobe_8mars2022.xlsx"]
rule get_stats_cirm_input:
        input:
                file="corpora/cirm/{file}"
        output:
                stats="corpora/cirm/stats/{file,.*xlsx}_stats.csv"
        params:
                c0="source",
                c1="uri",
                c2="valeur",
                c="line"
        run:
                import pandas
                df1 = pandas.read_excel(input.file)
                df2 = pandas.DataFrame({params.c0: str(input.file).split("/")[2], params.c1: [str(input.file)], params.c2: [len(df1.index)]})
                df2.to_csv(output.stats, index=False)


'''
cirm | nb taxa entites (bia) | count_lines(corpora/cirm/bia/bia_taxa.txt)
cirm | nb habitat entites (bia) | count_lines(corpora/cirm/bia/bia_habitats.txt)
cirm | nb taxa entites (levures) | count_lines(corpora/cirm/levures/levures_taxa.txt)
cirm | nb habitat entites (levures) | count_lines(corpora/cirm/levures/levures_habitats.txt)
cirm | nb taxa entites (cfbp) | count_lines(corpora/cirm/cfbp/cfbp_taxa.txt)
cirm | nb habitat entites (cfbp) | count_lines(corpora/cirm/cfbp/cfbp_habitats.txt)
'''
SORTIES_CIRM= ["bia/bia_taxa.txt", "bia/bia_habitats.txt", "levures/yeast_taxa.txt", "levures/yeast_habitats.txt", "cfbp/cfbp_taxa.txt", "cfbp/cfbp_habitats.txt"]
'''
get stats cirm
'''
rule get_stats_cirm_output:
        input:
                file="corpora/cirm/{file}"
        output:
                stats="corpora/cirm/stats/{file,.*txt}_stats.csv"
        params:
                c0="source",
                c1="uri",
                c2="valeur",
                c="line"
        run:
                import pandas
                df1 = pandas.read_csv(input.file, names = [params.c], header=None)
                df2 = pandas.DataFrame({params.c0: str(input.file).split("/")[2], params.c1: [str(input.file)], params.c2: [len(df1.index)]})
                df2.to_csv(output.stats, index=False)



FILES_CIRM = ENTREES_CIRM + SORTIES_CIRM
'''
merge
'''
rule merge_stats_cirm:
        input:
                files=expand("corpora/cirm/stats/{file}_stats.csv", file=FILES_CIRM)
        output:
                result="corpora/cirm/stats/stats.full.csv"
        run:
                import pandas
                frames = [ pandas.read_csv(f) for f in input.files ]
                result = pandas.concat(frames)
                result.to_csv(output.result, index=False)

'''
all meta
'''
rule all_meta:
        input:
		"corpora/cirm/stats/stats.full.csv"
