import time

# Save timestamp
start = time.time()

## config file
configfile: "config/config.yaml"

'''
## variables, check values into config file
_ontobiotope = config['ONTOBIOTOPE']
_names = config['NCBI_TAXO_NAMES']
_habitat_root = config['HABITAT_ROOT']
_phenotype_root = config['PHENOTYPE_ROOT']
_use_root = config['USE_ROOT']
'''

ONTONAMES = 'BioNLP-OST+EnovFood-Habitat BioNLP-OST+EnovFood-Phenotype BioNLP-OST+EnovFood-Use'


'''
all
'''
rule all:
	input:
		'corpora/florilege/alvisir/BioNLP-OST+EnovFood-Habitat.json', 
		'corpora/florilege/alvisir/BioNLP-OST+EnovFood-Phenotype.json',
		'corpora/florilege/alvisir/BioNLP-OST+EnovFood-Use.json',
		'share/BioNLP-OST+EnovFood-Habitat.tomap',
		'share/BioNLP-OST+EnovFood-Phenotype.tomap',
		'share/food-process-lexicon.txt',
		'share/NCBI_taxa_ontobiotope.txt',
		'share/BioNLP-OST+EnovFood-Habitat.obo',
		'share/BioNLP-OST+EnovFood-Phenotype.obo',
		'share/BioNLP-OST+EnovFood-Use.obo',
		expand("share/{ontoname}.paths", ontoname=ONTONAMES.split(' ')),
		#yatea_candidates='share/yatea/candidates.xml',
		#yatea_var_candidates='share/yatea-var/candidates.xml',
		yatea_ncbi= 'share/yatea-ncbi/candidates.xml'


'''
Remove obsolote concepts
'''
rule remove_obsolete_concepts:
	input:
		onto=config['ONTOBIOTOPE']
	output:
		onto='share/BioNLP-OST+EnovFood-no-obsolete.obo'
	conda: 'softwares/envs/obo-utils-env.yaml'	
	shell: 'python softwares/obo-utils/obo2obo.py {input.onto} > {output.onto}'


'''
Cut Habitat subtree
'''
rule cut_subtrees_habitat:
	input:
		onto='share/BioNLP-OST+EnovFood-no-obsolete.obo'
	output:
		onto='share/BioNLP-OST+EnovFood-Habitat.obo'
	params:
		habitat_root=config['HABITAT_ROOT']
	conda: 'softwares/envs/obo-utils-env.yaml'
	shell: """
			python softwares/obo-utils/obo-subtree.py \
			--default-exclude \
			--include-root {params.habitat_root} \
			{input.onto} > {output.onto}
			"""

'''
Cut Phenotype subtree
'''
rule cut_subtrees_phenotype:
	input:
		onto='share/BioNLP-OST+EnovFood-no-obsolete.obo'
	output:
		onto='share/BioNLP-OST+EnovFood-Phenotype.obo'
	params:
		phenotype_root=config['PHENOTYPE_ROOT']
	conda: 'softwares/envs/obo-utils-env.yaml'
	shell: """
			python softwares/obo-utils/obo-subtree.py \
			--default-exclude \
			--include-root {params.phenotype_root} \
			{input.onto} > {output.onto}
			"""
            
'''
Cut Use subtree
'''
rule cut_subtrees_use:
	input:
		onto='share/BioNLP-OST+EnovFood-no-obsolete.obo'
	output:
		onto='share/BioNLP-OST+EnovFood-Use.obo'
	conda: 'softwares/envs/obo-utils-env.yaml'
	params:
		use_root=config['USE_ROOT']
	shell: """
			python softwares/obo-utils/obo-subtree.py \
			--default-exclude \
			--include-root {params.use_root} \
			{input.onto} > {output.onto}
			"""


'''
Analyze ontologies with tomap
'''
rule analyze_onto_Habitat:
	input:
		onto='share/BioNLP-OST+EnovFood-Habitat.obo'
	output:
		tomap='share/BioNLP-OST+EnovFood-Habitat.tomap'
	params:
		plan='plans/biotope_ontology_analyzer.plan',
		tmpDir=config['TMP']
	singularity:config["SINGULARITY_IMG"]
	shell: """
			alvisnlp -cleanTmp -tmp {params.tmpDir} -verbose \
			-alias input {input.onto} \
			-alias output {output.tomap} \
			{params.plan}
			"""

'''
Analyze ontologies with tomap
'''
rule analyze_onto_phenotype:
	input:
		onto='share/BioNLP-OST+EnovFood-Phenotype.obo'
	output:
		tomap='share/BioNLP-OST+EnovFood-Phenotype.tomap'
	params:
		plan='plans/phenotype_ontology_analyzer.plan',
		tmpDir=config['TMP']
	singularity:config["SINGULARITY_IMG"]
	shell: """
			alvisnlp -J-Xmx8g -cleanTmp -tmp {params.tmpDir} -verbose \
			-alias input {input.onto} \
			-alias output {output.tomap} \
			{params.plan}
			"""


'''
Build food process lexicon
'''
rule build_food_process_lexicon:
	input:
		onto='share/BioNLP-OST+EnovFood-Habitat.obo'
	output:
		lexicon='share/food-process-lexicon.txt'
	params:
		plan='plans/food_process_lex.plan',
		tmpDir=config['TMP']
	singularity:config["SINGULARITY_IMG"]
	shell: """alvisnlp -cleanTmp -tmp {params.tmpDir} -verbose \
		-alias input {input.onto} \
		-alias output {output.lexicon} \
		{params.plan}
		"""

'''
Build NCBI common name lexicon
get ncbi names
'''
rule get_ncbi_names:
	input:
		names=config['NCBI_TAXO_NAMES']
	output:
		common_names='share/NCBI_common_names'
	shell: 'grep "common name" {input.names} |cut -f1,2 -d"|" |perl -npe "s/\s+\|\s+/\t/; s/\s+\n/\n/;" > {output.common_names}'

'''
build name lexicon
'''
rule build_ncbi_common_name_lexicon:
	input: 
		onto='share/BioNLP-OST+EnovFood-Habitat.obo',
		common_names='share/NCBI_common_names',
		tomap='share/BioNLP-OST+EnovFood-Habitat.tomap',
		graylist='share/graylist_extended.heads',
		emptywords='share/stopwords_EN.ttg'
	output:
		taxa='share/NCBI_taxa_ontobiotope.txt',
		#yatea_candidates='share/yatea/candidates.xml',
		#yatea_var_candidates='share/yatea-var/candidates.xml',
		yatea_ncbi= 'share/yatea-ncbi/candidates.xml'
	params:
		plan='plans/tag_ncbi_common_names.plan',
		tmpDir=config['TMP']
	singularity:config["SINGULARITY_IMG"]
	shell: """alvisnlp -J-Xmx8g -J-XX:+UseSerialGC -cleanTmp -tmp {params.tmpDir} -verbose \
	-alias input {input.common_names} \
	-alias output {output.taxa} \
	-alias ontobiotope {input.onto} \
	-xalias '<ontobiotope-tomap empty-words="{input.emptywords}" graylist="{input.graylist}" whole-proxy-distance="false">{input.tomap}</ontobiotope-tomap>' \
	{params.plan}
	"""

'''
obo to json
'''
rule convert_obo2json_habitat:
	input:
		obo='share/BioNLP-OST+EnovFood-Habitat.obo'
	output:
		json='corpora/florilege/alvisir/BioNLP-OST+EnovFood-Habitat.json'
	params:
		habitat_root=config['HABITAT_ROOT']
	conda: 'softwares/envs/obo-utils-env.yaml'
	shell: 'python softwares/obo-utils/obo2json.py --root {params.habitat_root} {input.obo} > {output.json}'

'''
convert phenotype results to json
'''
rule convert_obo2json_phenotype:
	input:
		obo='share/BioNLP-OST+EnovFood-Phenotype.obo'
	output:
		json='corpora/florilege/alvisir/BioNLP-OST+EnovFood-Phenotype.json'
	params:
		phenotype_root=config['PHENOTYPE_ROOT']
	conda: 'softwares/envs/obo-utils-env.yaml'
	shell: 'python softwares/obo-utils/obo2json.py --root {params.phenotype_root} {input.obo} > {output.json}'


'''
convert use results to json
'''
rule convert_obo2json_use:
	input:
		obo='share/BioNLP-OST+EnovFood-Use.obo'
	output:
		json='corpora/florilege/alvisir/BioNLP-OST+EnovFood-Use.json'
	params:
		use_root=config['USE_ROOT']
	conda: 'softwares/envs/obo-utils-env.yaml'
	shell: 'python softwares/obo-utils/obo2json.py --root {params.use_root} {input.obo} > {output.json}'



'''
generate concept paths
'''
rule generate_concept_path:
	input:
		onto="share/{ontoname}.obo"
	output:
		paths="share/{ontoname}.paths"
	params:
		plan="plans/get_onto_paths.plan",
		tmpDir=config['TMP']
	singularity:config['SINGULARITY_IMG']
	shell: """alvisnlp -cleanTmp -tmp {params.tmpDir} -verbose \
		-alias input {input.onto} \
		-alias output {output.paths} \
		 {params.plan}
		"""

# Save timestamp
end = time.time()
print(end - start)