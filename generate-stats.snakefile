#localrules: stats_cirm_BIA

'''
all
'''
rule all:
	input:
		result="corpora/florilege/stats.full.csv",
		result_labels="corpora/florilege/full_stats_with_labels.csv"



SOURCES=["cirm", "genbank", "dsmz", "pubmed", "BioNLP-OST-2019"]

'''
cirm | nb entrees | count_lines(corpora/cirm/BIA_2021/florilege_export_final_17_02_21.xlsx)
cirm | nb yeast entrees | count_lines(corpora/cirm/Levures_2021/Florilege_21012021.xlsx)
'''
ENTREES_CIRM = ["BIA_2021/florilege_export_final_17_02_21.xlsx", "Levures_2021/Florilege_21012021.xlsx"]
rule stats_cirm_BIA:
	input:
		file="corpora/cirm/{file}"
	output:
		stats="corpora/cirm/stats/{file,.*xlsx}_stats.csv"
	params:
		result="cirm/{file}",
		c0="source",
		v0="cirm",
		c1="uri",
		c2="valeur",
		c="line"
	run:
		import pandas
		df1 = pandas.read_excel(input.file)
		df2 = pandas.DataFrame({params.c0: params.v0, params.c1: [params.result], params.c2: [len(df1.index)]})
		df2.to_csv(output.stats, index=False)

'''
cirm | nb entites | count_lines(corpora/cirm/mapped_taxids.txt)
cirm | nb yeast entities | count_lines(corpora/cirm/mapped_yeast_taxa.txt)
cirm | nb entites | count_lines(corpora/cirm/mapped_habitats.txt)
cirm | nb yeast habitats | count_lines(corpora/cirm/mapped_yeast_habitats.txt)
'''
SORTIES_CIRM= ["mapped_taxids.txt", "mapped_yeast_taxa.txt", "mapped_habitats.txt", "mapped_yeast_habitats.txt" ]
'''
'''
rule stats_cirm_Levure:
	input:
		file="corpora/cirm/{file}"
	output:
		stats="corpora/cirm/stats/{file,.*txt}_stats.csv"
	params:
		result="cirm/{file}",
		c0="source",
		v0="cirm",
		c1="uri",
		c2="valeur",
		c="line"
	run:
		import pandas
		df1 = pandas.read_csv(input.file, names = [params.c], header=None)
		df2 = pandas.DataFrame({params.c0: params.v0, params.c1: [params.result], params.c2: [len(df1.index)]})
		df2.to_csv(output.stats, index=False)


FILES_CIRM = ENTREES_CIRM + SORTIES_CIRM
'''
merge
'''
rule merge_stats_cirm:
	input:
		files=expand("corpora/cirm/stats/{file}_stats.csv", file=FILES_CIRM)
	output:
		result="corpora/cirm/stats/stats.full.csv"
	run:
		import pandas
		frames = [ pandas.read_csv(f) for f in input.files ]
		result = pandas.concat(frames)
		result.to_csv(output.result, index=False)

'''
genbank | nb entrees | count_lines(corpora/genbank/corpora/genbank/GenBank_extraction_20210127.tsv)
genbank | nb entites | count_lines(corpora/genbank/mapped_taxids.txt)
genbank | nb habitats | count_lines(corpora/genbank/mapped_habitats.txt)
'''
ENTREES_GENBANK = ["GenBank_extraction_20210127.tsv"]
SORTIES_GENBANK = ["mapped_taxids.txt", "mapped_habitats.txt" ]
FILES_GENBANK = ENTREES_GENBANK + SORTIES_GENBANK
'''
'''
rule stats_genbank:
	input:
		file="corpora/genbank/{file}"
	output:
		stats="corpora/genbank/stats/{file}_stats.csv"
	params:
		result="genbank/{file}",
		c0="source",
		v0="genbank",
		c1="uri",
		c2="valeur",
		c="line"
	run:
		import pandas
		df1 = pandas.read_csv(input.file, names = [params.c], header=None)
		df2 = pandas.DataFrame({params.c0: params.v0, params.c1: [params.result], params.c2: [len(df1.index)]})
		df2.to_csv(output.stats, index=False)

'''
merge
'''
rule merge_stats_genbank:
	input:
		files=expand("corpora/genbank/stats/{file}_stats.csv", file=FILES_GENBANK)
	output:
		result="corpora/genbank/stats/stats.full.csv"
	run:
		import pandas
		frames = [ pandas.read_csv(f) for f in input.files ]
		result = pandas.concat(frames)
		result.to_csv(output.result, index=False)

'''
dsmz | nb entrees | count_lines(corpora/dsmz/dsmz-data/category=from_ncbi_taxonomy-key=taxid.tsv)
dsmz | nb entites | count_lines(corpora/dsmz/mapped_taxids.txt)
dsmz | nb habitats | count_lines(corpora/dsmz/mapped_habitats.txt)
'''
ENTREES_DSMZ = ["dsmz-data/category=from_ncbi_taxonomy-key=taxid.tsv" ]
SORTIES_DSMZ = ["mapped_taxids.txt", "mapped_habitats.txt" ]
FILES_DSMZ = ENTREES_DSMZ + SORTIES_DSMZ
'''
'''
rule stats_dsmz:
	input:
		file="corpora/dsmz/{file}"
	output:
		stats="corpora/dsmz/stats/{file}_stats.csv"
	params:
		result="dsmz/{file}",
		c0="source",
		v0="dsmz",
		c1="uri",
		c2="valeur",
		c="line"
	run:
		import pandas
		df1 = pandas.read_csv(input.file, names = [params.c], header=None)
		df2 = pandas.DataFrame({params.c0: params.v0, params.c1: [params.result], params.c2: [len(df1.index)]})
		df2.to_csv(output.stats, index=False)

'''
merge
'''
rule merge_stats_dsmz:
	input:
		files=expand("corpora/dsmz/stats/{file}_stats.csv", file=FILES_DSMZ)
	output:
		result="corpora/dsmz/stats/stats.full.csv"
	run:
		import pandas
		frames = [ pandas.read_csv(f) for f in input.files ]
		result = pandas.concat(frames)
		result.to_csv(output.result, index=False)



'''
Pubmed | nb articles | count_files(corpora/pubmed/batches/*) x 1000
Pubmed | nb habitats | count_lines(corpora/pubmed/habitats.full.txt)             
Pubmed | nb relations | count_lines(corpora/pubmed/relations.full.txt)
pubmed | nb microorganisms | count_lines(corpora/pubmed/microorganisms.full.txt)     
Pubmed | nb uses | count_lines(corpora/pubmed/uses.full.txt)
Pubmed | nb phenotype-relations | count_lines(corpora/pubmed/phenotype-relations.full.txt)
Pubmed | nb uses-relations | count_lines(corpora/pubmed/uses-relations.full.txt)
Pubmed | nb phenotypes | count_lines(corpora/pubmed/phenotypes.full.txt)
'''
ENTREES_PUBMED = ["list_of_batches.txt"]
SORTIES_PUBMED = ["relations.full.txt", "phenotype-relations.full.txt", "uses-relations.full.txt", "microorganisms.full.txt", "habitats.full.txt", "phenotypes.full.txt", "uses.full.txt"]
FILES_PUBMED = ENTREES_PUBMED + SORTIES_PUBMED


'''
'''

rule stats_pubmed:
	input:
		file="corpora/pubmed/{file}"
	output:
		stats="corpora/pubmed/stats/{file}_stats.csv"
	params:
		result="pubmed/{file}",
		c0="source",
		v0="pubmed",
		c1="uri",
		c2="valeur",
		c="line"
	run:
		import pandas
		df1 = pandas.read_csv(input.file, names = [params.c], header=None)
		df2 = pandas.DataFrame({params.c0: params.v0, params.c1: [params.result], params.c2: [len(df1.index)]})
		df2.to_csv(output.stats, index=False)

'''
merge
'''
rule merge_stats_pubmed:
	input:
		files=expand("corpora/pubmed/stats/{file}_stats.csv", file=FILES_PUBMED)
	output:
		result="corpora/pubmed/stats/stats.full.csv"
	run:
		import pandas
		frames = [ pandas.read_csv(f) for f in input.files ]
		result = pandas.concat(frames)
		result.to_csv(output.result, index=False)



'''
eval_001, corpus utilisés, BioNLP-OST-2019
eval_002, date, None
eval_BB19-norm+ner_001, mesure pour l'evaluation de BB19-norm+ner,BioNLP-OST-2019/BB19-norm+ner/eval.json#eval_BB19-norm+ner_001
eval_BB19-norm+ner_002, score sur la prédiction des taxons de BB19-norm+ner,BioNLP-OST-2019/BB19-norm+ner/eval.json#eval_BB19-norm+ner_002
eval_BB19-norm+ner_003, score sur la prédiction des phénotypes de BB19-norm+ner,BioNLP-OST-2019/BB19-norm+ner/eval.json#eval_BB19-norm+ner_003
eval_BB19-norm+ner_004, score sur la prédiction des habitats de BB19-norm+ner,BioNLP-OST-2019/BB19-norm+ner/eval.json#eval_BB19-norm+ner_004
eval_BB19-rel+ner_001, mesure pour l'evaluation de BB19-rel+ner,BioNLP-OST-2019/BB19-rel+ner/eval.json#eval_BB19-rel+ner_001
eval_BB19-rel+ner_002, score sur la prédiction des Lives-In de BB19-rel+ner,BioNLP-OST-2019/BB19-rel+ner/eval.json#eval_BB19-rel+ner_002
eval_BB19-rel+ner_003, score sur la prédiction des Exhibits de BB19-rel+ner,BioNLP-OST-2019/BB19-rel+ner/eval.json#eval_BB19-rel+ner_003
eval_BB19-kb+ner_001, mesure pour l'evaluation de BB19-kb+ner,BioNLP-OST-2019/BB19-kb+ner/eval.json#eval_BB19-kb+ner_001#eval_BB19-kb+ner_002
eval_BB19-kb+ner_002, score moyen sur BB19-kb+ner,BioNLP-OST-2019/BB19-kb+ner/eval.json#eval_BB19-kb+ner_002
'''
ENTREES_EVAL = ["BioNLP-OST-2019/batches/BB19-norm+ner", "BioNLP-OST-2019/batches/BB19-rel+ner", "BioNLP-OST-2019/batches/BB19-kb+ner"]
SORTIES_EVAL = ["BioNLP-OST-2019/batches/BB19-norm+ner/eval.json", "BioNLP-OST-2019/batches/BB19-rel+ner/eval.json", "BioNLP-OST-2019/batches/BB19-kb+ner/eval.json"]
FILES_EVAL = ["BB19-norm+ner", "BB19-rel+ner", "BB19-kb+ner"]

def get_score_stats(file, entity):
	import json
	data = json.load(open(file))
	for d in (data['evaluation'])['global-evaluations']:
		scorings = d['scorings']
		for s in scorings:
			if s['name'] == entity:
				return s['measures'][0]
	return None


'''

'''
rule stats_eval_BB19_norm:
	input:
		file="corpora/BioNLP-OST-2019/batches/BB19-norm+ner/eval.json"
	output:
		stats="corpora/BioNLP-OST-2019/stats/BB19-norm+ner_stats.csv"
	params:
		result="BioNLP-OST-2019/BB19-norm+ner",
		c0="source",
		v0="bb19-norm+ner",
		c1="uri",
		c2="valeur",
		c="line"
	run:
		import pandas
		s = get_score_stats(input.file, "Standard scoring")
		h = get_score_stats(input.file, "Habitats")
		p = get_score_stats(input.file, "Phenotypes")
		m = get_score_stats(input.file, "Microorganisms")
		df2 = pandas.DataFrame({params.c0: [params.v0, params.v0, params.v0, params.v0, params.v0], params.c1: [params.result+"#Mesure", params.result+"#Standard_scoring", params.result+"#Habitat", params.result+"#Phenotype", params.result+"#Microorganism"], params.c2: [h['name'], s['value'], h['value'], p['value'], m['value']]})
		df2.to_csv(output.stats, index=False)


'''

'''
rule stats_eval_BB19_rel:
	input:
		file="corpora/BioNLP-OST-2019/batches/BB19-rel+ner/eval.json"
	output:
		stats="corpora/BioNLP-OST-2019/stats/BB19-rel+ner_stats.csv"
	params:
		result="BioNLP-OST-2019/BB19-rel+ner",
		c0="source",
		v0="bb19-rel+ner",
		c1="uri",
		c2="valeur",
		c="line"
	run:
		import pandas
		#s = get_score_stats(input.file, "Standard scoring")
		l = get_score_stats(input.file, "Lives_In")
		e = get_score_stats(input.file, "Exhibits")
		#df2 = pandas.DataFrame({params.c0: [params.v0, params.v0, params.v0, params.v0], params.c1: [params.result+"#Mesure", params.result+"#Standard_scoring", params.result+"#Lives_In", params.result+"#Exhibits"], params.c2: [l['name'], s['value'], l['value'], e['value']]})
		df2 = pandas.DataFrame({params.c0: [params.v0, params.v0, params.v0], params.c1: [params.result+"#Mesure", params.result+"#Lives_In", params.result+"#Exhibits"], params.c2: [l['name'], l['value'], e['value']]})
		df2.to_csv(output.stats, index=False)


'''

'''
rule stats_eval_BB19_kb:
	input:
		file="corpora/BioNLP-OST-2019/batches/BB19-kb+ner/eval.json"
	output:
		stats="corpora/BioNLP-OST-2019/stats/BB19-kb+ner_stats.csv"
	params:
		result="BioNLP-OST-2019/BB19-kb+ner",
		c0="source",
		v0="bb19-kb+ner",
		c1="uri",
		c2="valeur",
		c="line"
	run:
		import pandas
		s = get_score_stats(input.file, "Standard scoring")
		df2 = pandas.DataFrame({params.c0: [params.v0, params.v0], params.c1: [params.result+"#Mesure", params.result+"#Standard_scoring"], params.c2: [s['name'], s['value']]})
		df2.to_csv(output.stats, index=False)

'''
merge
'''
rule merge_stats_eval:
	input:
		files=expand("corpora/BioNLP-OST-2019/stats/{file}_stats.csv", file=FILES_EVAL)
	output:
		result="corpora/BioNLP-OST-2019/stats/stats.full.csv"
	run:
		import pandas
		frames = [ pandas.read_csv(f) for f in input.files ]
		result = pandas.concat(frames)
		result.to_csv(output.result, index=False)

'''
merge all
'''
rule merge_all:
	input:
		files=expand("corpora/{source}/stats/stats.full.csv", source=SOURCES)
	output:
		result="corpora/florilege/stats.full.csv"
	run:
		import pandas
		frames = [ pandas.read_csv(f) for f in input.files ]
		result = pandas.concat(frames)
		result.to_csv(output.result, index=False)



'''
merge
'''
rule joint_stats:
	input:
		full_r="corpora/florilege/stats.full.csv",
		concepts="corpora/florilege/labels.stats"
	output:
		result="corpora/florilege/full_stats_with_labels.csv"
	run:
		import pandas
		df1=pandas.read_csv(input.concepts)
		df2=pandas.read_csv(input.full_r)
		df = pandas.merge(df1, df2, on="uri", how="left")
		df.to_csv(output.result, index=False)
