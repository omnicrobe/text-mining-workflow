## local rule
# localrules: all, concat_results

## config file
configfile: "config/config.yaml"

'''
## variables, check values into config file
_ontobiotope = config['ONTOBIOTOPE']
_names = config['NCBI_TAXO_NAMES']
_pubmed_batches_home = config['PUBMED_BATCHES_HOME']
_pubmed_xslt_file = config['PUBMED_XSLT_FILE']
_ncbi_taxo_microorganisms = config['NCBI_TAXO_MICROORGANISMS']
_ncbi_taxo_id = config['NCBI_TAXO_ID']
_ncbi_taxo_and_id_microorganisms = config['NCBI_TAXO_AND_ID_MICROORGANISMS']
'''


## document batches
BATCHES, = glob_wildcards(config["PUBMED_BATCHES_HOME"] + "/{id}/batch.xml")

## list of the results
RESULTS = ["relations", "phenotype-relations", "uses-relations", "microorganisms", "habitats", "phenotypes", "uses"]


'''
all
'''
rule all:
    input:
        relations=expand(config["PUBMED_BATCHES_HOME"] + "/{B}/relations.txt", B=BATCHES),
        phenotypeRelations=expand(config["PUBMED_BATCHES_HOME"] + "/{B}/phenotype-relations.txt", B=BATCHES),
        usesRelations=expand(config["PUBMED_BATCHES_HOME"] + "/{B}/uses-relations.txt", B=BATCHES),
        microorganisms=expand(config["PUBMED_BATCHES_HOME"] + "/{B}/microorganisms.txt", B=BATCHES),
        habitats=expand(config["PUBMED_BATCHES_HOME"] + "/{B}/habitats.txt", B=BATCHES),
        phenotypes=expand(config["PUBMED_BATCHES_HOME"] + "/{B}/phenotypes.txt", B=BATCHES),
        uses=expand(config["PUBMED_BATCHES_HOME"] + "/{B}/uses.txt", B=BATCHES),
        index=expand(config["PUBMED_BATCHES_HOME"] + "/{B}/index", B=BATCHES),
        index_folder="corpora/florilege/alvisir/index",
        expander_folder="corpora/florilege/alvisir/expander",
        florilege_Habitat_result="corpora/florilege/pubmed/PubMed-Habitat.txt",
        florilege_Phenotype_result="corpora/florilege/pubmed/PubMed-Phenotype.txt",
        florilege_Use_result="corpora/florilege/pubmed/PubMed-Use.txt",
        result=expand("corpora/florilege/pubmed/{R}.full.txt", R=RESULTS)


'''
Extract entities in different corpus 
batches using the alvisnlp plan (omnicrobe_main.plan)
'''
rule run_pubmed_main:
    input:
        file = config['PUBMED_BATCHES_HOME'] + "/{B}/batch.xml",
        xslt = config['PUBMED_XSLT_FILE']
    output:
        results=expand(config["PUBMED_BATCHES_HOME"] + "/{{B}}/{R}.txt", R=RESULTS),
        index=directory(config["PUBMED_BATCHES_HOME"] + "/{B}/index"),
        yatea_candidates=config["PUBMED_BATCHES_HOME"] + "/{B}/yatea/candidates.xml",
        yatea_var_candidates=config["PUBMED_BATCHES_HOME"] + "/{B}/yatea-var/candidates.xml"
    params:
        batch="{B}",
        corpus='pubmed',
        onto_habitat='share/BioNLP-OST+EnovFood-Habitat.obo',
        tomap_habitat='share/BioNLP-OST+EnovFood-Habitat.tomap',
        onto_pheno='share/BioNLP-OST+EnovFood-Phenotype.obo',
        tomap_pheno='share/BioNLP-OST+EnovFood-Phenotype.tomap',
        graylist='share/graylist_extended.heads',
        emptywords='share/stopwords_EN.ttg',
        onto_use='share/BioNLP-OST+EnovFood-Use.obo',
        plan='plans/omnicrobe_main.plan',
        dir=config["PUBMED_BATCHES_HOME"] + "/{B}/",
        taxid_microorganisms=config['NCBI_TAXO_MICROORGANISMS'],
        taxa_id_full=config['NCBI_TAXO_ID'],
        dummy= config["PUBMED_BATCHES_HOME"] + '/{B}/bionlp-st',
        log="alvisnlp.log"
    singularity:config["SINGULARITY_IMG"]
    shell:"""
        rm -f {output.yatea_candidates} {output.yatea_var_candidates} && mkdir -p {params.dummy} && alvisnlp -J-XX:+UseSerialGC -J-Xmx30g -cleanTmp -verbose \
        -log {params.log} \
        -alias format pubmed \
        -alias input {input.file} \
        -alias input-xslt {input.xslt} \
        -alias batch batch={params.batch} \
        -outputDir {params.dir} \
        -alias ontobiotope-habitat {params.onto_habitat} \
        -xalias '<ontobiotope-tomap-habitat empty-words="{params.emptywords}" graylist="{params.graylist}" whole-proxy-distance="false">{params.tomap_habitat}</ontobiotope-tomap-habitat>' \
        -alias ontobiotope-phenotypes {params.onto_pheno} \
        -xalias '<ontobiotope-tomap-phenotypes empty-words="{params.emptywords}" whole-proxy-distance="false">{params.tomap_pheno}</ontobiotope-tomap-phenotypes>' \
        -alias ontobiotope-use {params.onto_use} \
        -alias taxid_microorganisms {params.taxid_microorganisms} \
        -alias taxa+id_full {params.taxa_id_full} \
        {params.plan}
        """


'''
select results to concat

rule selectToConcat:
    input:
        results=expand(config["PUBMED_BATCHES_HOME"] + "/{{B}}/{R}.txt", R=RESULTS),
        index=config["PUBMED_BATCHES_HOME"] + "/{B}/index"
    output:
        results=config["PUBMED_BATCHES_HOME"] + "/{B}/{R}.txt"
'''


'''
concat the different results
for 
* relations 
* phenotype-relations 
* uses-relations 
* microorganisms 
* habitats 
* phenotypes 
* uses
/!\ bash arguments too long if you use cat
'''
rule concat_results:
    input: 
        expand(config["PUBMED_BATCHES_HOME"] + "/{B}/{{R}}.txt", B=BATCHES)
    output:
        result="corpora/florilege/pubmed/{R}.full.txt"
    run:
        with open(output.result, 'w') as out:
            for fname in input:
                with open(fname) as infile:
                    out.write(infile.read())

'''
select files to be formated
'''
rule select:
    input:
        files=expand("corpora/florilege/pubmed/{R}.full.txt", R=RESULTS)
    output:
        relation="corpora/florilege/pubmed/relations.full.txt",
                phenotype_relations="corpora/florilege/pubmed/phenotype-relations.full.txt",
                use_relations="corpora/florilege/pubmed/uses-relations.full.txt"
    shell:"""
        """
            


'''
merge indexes from the batches
'''
rule merge_pubmed_index:
    input:
        index=expand(config["PUBMED_BATCHES_HOME"] +"/{B}/index", B=BATCHES)
    output:
        index_folder=directory("corpora/florilege/alvisir/index")
    params:
        alvisir=config["ALVISIR_HOME"]
    shell: """
        java -cp {params.alvisir}/lib/lucene-core-3.6.1.jar:{params.alvisir}/lib/lucene-misc-3.6.1.jar \
        org.apache.lucene.misc.IndexMergeTool \
        {output.index_folder} {input.index}
        """



'''
create the expander
'''
rule create_pubmed_expander:
    input:
        expander="share/expander.xml",
        taxa_id_microorganisms=config['NCBI_TAXO_AND_ID_MICROORGANISMS'],
                onto_habitat="share/BioNLP-OST+EnovFood-Habitat.obo",
        onto_phenotype="share/BioNLP-OST+EnovFood-Phenotype.obo",
        onto_use="share/BioNLP-OST+EnovFood-Use.obo"
    output:
        expander_folder=directory("corpora/florilege/alvisir/expander")
    params:
        alvisir=config["ALVISIR_HOME"]
    shell:"""
        {params.alvisir}/bin/alvisir-index-expander {output.expander_folder} {input.expander}
          """


'''
formatting the extracted *relations* for 
integration in Florilege
'''
rule format_pubmed_relations:
    input:
        file="corpora/florilege/pubmed/relations.full.txt"
    output:
        florilege_result="corpora/florilege/pubmed/PubMed-Habitat.txt"
    conda: 'softwares/envs/python3_env.yaml'
    shell:"""
        python softwares/scripts/format-pubmed-results.py \
        --pubmed-results {input.file} \
        > {output.florilege_result}
          """


'''
formatting the extracted *phenotype relations* for 
integration in Florilege
'''
rule format_pubmed_phenotype_relations:
    input:
        file="corpora/florilege/pubmed/phenotype-relations.full.txt"
    output:
        florilege_result="corpora/florilege/pubmed/PubMed-Phenotype.txt"
    conda: 'softwares/envs/python3_env.yaml'
    shell:"""
        python softwares/scripts/format-pubmed-results.py \
        --pubmed-results {input.file} \
        > {output.florilege_result}
          """

'''
formatting the extracted *use relations* for 
integration in Florilege
'''
rule format_pubmed_use_relations:
    input:
        file="corpora/florilege/pubmed/uses-relations.full.txt"
    output:
        florilege_result="corpora/florilege/pubmed/PubMed-Use.txt"
    conda: 'softwares/envs/python3_env.yaml'
    shell:"""
        python softwares/scripts/format-pubmed-results.py \
        --pubmed-results {input.file} \
        > {output.florilege_result}
          """




'''-------------------------THIS PART TO GET META INFO-----------------------------------------------'''


rule list_batches:
    input:
        file="corpora/pubmed/batches"
    output:
        batches="corpora/florilege/pubmed/list_of_batches.txt"
    shell:"""
        ls {input.file} > {output.batches}
        """


'''
Pubmed | nb articles | count_files(corpora/pubmed/batches/*) x 1000
Pubmed | nb habitats | count_lines(corpora/florilege/pubmed/habitats.full.txt)
Pubmed | nb relations | count_lines(corpora/florilege/pubmed/relations.full.txt)
pubmed | nb microorganisms | count_lines(corpora/florilege/pubmed/microorganisms.full.txt)
Pubmed | nb uses | count_lines(corpora/florilege/pubmed/uses.full.txt)
Pubmed | nb phenotype-relations | count_lines(corpora/florilege/pubmed/phenotype-relations.full.txt)
Pubmed | nb uses-relations | count_lines(corpora/florilege/pubmed/uses-relations.full.txt)
Pubmed | nb phenotypes | count_lines(corpora/florilege/pubmed/phenotypes.full.txt)
'''
ENTREES_PUBMED = ["pubmed/list_of_batches.txt"]
SORTIES_PUBMED = ["pubmed/relations.full.txt", "pubmed/phenotype-relations.full.txt", "pubmed/uses-relations.full.txt", "pubmed/microorganisms.full.txt", "pubmed/habitats.full.txt", "pubmed/phenotypes.full.txt", "pubmed/uses.full.txt"]
FILES_PUBMED = ENTREES_PUBMED + SORTIES_PUBMED


'''
get stats
'''
rule get_stats_pubmed:
        input:
                file="corpora/florilege/{file}"
        output:
                stats="corpora/pubmed/stats/{file}_stats.csv"
        params:
                c0="source",
                c1="uri",
                c2="valeur",
                c="line"
        run:
                import pandas
                df1 = pandas.read_csv(input.file, names = [params.c], header=None)
                df2 = pandas.DataFrame({params.c0: str(input.file).split("/")[0], params.c1: [str(input.file)], params.c2: [len(df1.index)]})
                df2.to_csv(output.stats, index=False)



'''
merge
'''
rule merge_stats_pubmed:
        input:
                files=expand("corpora/pubmed/stats/{file}_stats.csv", file=FILES_PUBMED)
        output:
                result="corpora/pubmed/stats/stats.full.csv"
        run:
                import pandas
                frames = [ pandas.read_csv(f) for f in input.files ]
                result = pandas.concat(frames)
                result.to_csv(output.result, index=False)


'''
all meta
'''
rule all_meta:
    input:
        "corpora/pubmed/stats/stats.full.csv"
