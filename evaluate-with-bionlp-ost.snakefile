## local rule
# localrules: all, concat_results

## config file
configfile: "config/config.yaml"

## document batches
BATCHES, = glob_wildcards(config["BIONLPOST_BATCHES_HOME"] + "/{id}/bionlp-st")


'''
all
'''
rule all:
    input:
        log=expand(config["BIONLPOST_BATCHES_HOME"] + "/{B}/eval.json", B=BATCHES),
        scores=expand(config["BIONLPOST_BATCHES_HOME"] + "/{B}/{B}-eval.json", B=BATCHES)
        


'''
Extract entities in different corpus 
batches using the alvisnlp plan (omnicrobe_main.plan)
'''
rule run_bionlp_prediction:
    input:
        dir=directory(config["BIONLPOST_BATCHES_HOME"] + "/{B}/bionlp-st"),
        xslt=config["PUBMED_XSLT_FILE"]
    output:
        relations=config["BIONLPOST_BATCHES_HOME"] + "/{B}/relations.txt",
        phenotypeRelations=config["BIONLPOST_BATCHES_HOME"] + "/{B}/phenotype-relations.txt",
        usesRelations=config["BIONLPOST_BATCHES_HOME"] + "/{B}/uses-relations.txt",
        microorganisms=config["BIONLPOST_BATCHES_HOME"] + "/{B}/microorganisms.txt",
        habitats=config["BIONLPOST_BATCHES_HOME"] + "/{B}/habitats.txt",
        phenotypes=config["BIONLPOST_BATCHES_HOME"] + "/{B}/phenotypes.txt",
        uses=config["BIONLPOST_BATCHES_HOME"] + "/{B}/uses.txt",
        index=directory(config["BIONLPOST_BATCHES_HOME"] + "/{B}/index"),
        a2=directory(config["BIONLPOST_BATCHES_HOME"] + "/{B}/a2"),
        yatea_candidates=config["BIONLPOST_BATCHES_HOME"] + "/{B}/yatea/candidates.xml",
        yatea_var_candidates=config["BIONLPOST_BATCHES_HOME"] + "/{B}/yatea-var/candidates.xml"
    log:config["BIONLPOST_BATCHES_HOME"] + "/{B}/alvisnlp.log"
    params:
        batch="{B}",
        corpus='BioNLP-OST-2019',
        onto_habitat=config["ONTOBIOTOPE_HABITAT"],
        tomap_habitat='share/BioNLP-OST+EnovFood-Habitat.tomap',
        onto_pheno=config["ONTOBIOTOPE_PHENOTYPE"],
        tomap_pheno='share/BioNLP-OST+EnovFood-Phenotype.tomap',
        graylist='share/graylist_extended.heads',
        emptywords='share/stopwords_EN.ttg',
        ontobiotopeUse=config["ONTOBIOTOPE_USE"],
        plan='plans/omnicrobe_main.plan',
        dir='corpora/BioNLP-OST-2019/batches/{B}/',
        taxid_microorganisms='share/extended-microorganisms-taxonomy/taxid_microorganisms.txt',
        taxa_id_full='share/extended-microorganisms-taxonomy/taxa+id_full.txt'
    singularity:config["SINGULARITY_IMG"]
    shell:"""
        rm -f {output.yatea_candidates} {output.yatea_var_candidates} && alvisnlp -J-XX:+UseSerialGC -J-Xmx25g -cleanTmp -verbose \
        -log {log} \
        -alias format bionlp-st \
        -alias input-dir {input.dir} \
        -outputDir {params.dir} \
        -alias ontobiotope-habitat {params.onto_habitat} \
        -xalias '<ontobiotope-tomap-habitat empty-words="{params.emptywords}" graylist="{params.graylist}" whole-proxy-distance="false">{params.tomap_habitat}</ontobiotope-tomap-habitat>' \
        -alias ontobiotope-phenotypes {params.onto_pheno} \
        -xalias '<ontobiotope-tomap-phenotypes empty-words="{params.emptywords}" whole-proxy-distance="false">{params.tomap_pheno}</ontobiotope-tomap-phenotypes>' \
        -alias ontobiotope-use {params.ontobiotopeUse} \
        -alias taxid_microorganisms {params.taxid_microorganisms} \
        -alias taxa+id_full {params.taxa_id_full} \
        {params.plan}        
        """

rule archive_prediction:
    input:
        a2=directory(config["BIONLPOST_BATCHES_HOME"] + "/{B}/a2")
    output:
        zip=config["BIONLPOST_BATCHES_HOME"] + "/{B}/predictions.zip"
    shell:
        """zip -9 {output.zip} {input.a2}/*.a2"""

        
rule evaluate:
    input:
        zip=config["BIONLPOST_BATCHES_HOME"] + "/{B}/predictions.zip"
    output:
        scores=config["BIONLPOST_BATCHES_HOME"] + "/{B}/{B}-eval.json"
    params:
        api=config["BIONLPOST_API"],
        task="{B}"
    shell:
        """curl -o {output.scores} -X POST "{params.api}/task/{params.task}/test/evaluate" -H "accept: application/json" -H "Content-Type: multipart/form-data" -F "resamples=0" -F "detailed=false" -F "alternate=true" -F "zipfile=@{input.zip};type=application/zip" """


