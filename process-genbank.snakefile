## Processing data sources GenBank

## config file
configfile: 'config/config.yaml'

'''
## variables, check values into config file
_ontobiotope = config['ONTOBIOTOPE']
_names = config['NCBI_TAXO_NAMES']
_pubmed_batches_home = config['PUBMED_BATCHES_HOME']
_pubmed_xslt_file = config['PUBMED_XSLT_FILE']
_ncbi_taxo_microorganisms = config['NCBI_TAXO_MICROORGANISMS']
_ncbi_taxo_id = config['NCBI_TAXO_ID']
_ncbi_taxo_and_id_microorganisms = config['NCBI_TAXO_AND_ID_MICROORGANISMS']
_genbank_db = config['GENBANK_DB']
'''


''' 
all
'''
rule all:
	input:
		'corpora/florilege/genbank/genbank-results.txt'


'''
extract genbank data
'''
rule extract_genbank_data:
	input:
		taxa_microorganisms=config['NCBI_TAXO_AND_ID_MICROORGANISMS'],
		db_path=config['GENBANK_DB']
	output:
		genbank_data='corpora/genbank/GenBank_extraction_full.tsv'
	conda:'softwares/envs/biopython-env.yaml'
	shell: 'python3 softwares/scripts/extractGB.py --taxoref {input.taxa_microorganisms} --dbpath {input.db_path} --fout {output.genbank_data}'


'''
get taxa /!\ need gawk to be installed
'''
rule get_genbank_taxa:
	input:
		file='corpora/genbank/GenBank_extraction_full.tsv'
	output:
		taxa='corpora/genbank/taxids.txt'
	shell: """cut -f5 {input.file} |sort -u > {output.taxa}"""

'''
get habitats
'''
rule get_genbank_habitat:
	input:
		file='corpora/genbank/GenBank_extraction_full.tsv'
	output:
		habitats='corpora/genbank/habitats.txt'
	shell: """awk -F'\\t' '{{if ($7 && $7 != " ") print $7; if ($8 && $8 != " ") print $8;}}' {input.file} |sort -u > {output.habitats}"""

'''
map taxid
'''
rule map_genbank_taxid:
	input:
		taxids='corpora/genbank/taxids.txt'
	output:
		mapped_taxids='corpora/genbank/mapped_taxids.txt'
	params:
		taxa_id_full=config['NCBI_TAXO_ID'],
		plan='plans/map_taxid.plan'
	singularity:config["SINGULARITY_IMG"]
	shell: """alvisnlp -J-Xmx32g -cleanTmp -verbose \
	-alias input {input.taxids} \
	-alias taxa+id_full {params.taxa_id_full} \
	-alias output {output.mapped_taxids} \
	{params.plan}
	"""


'''
map habitats of microorganisms
'''
rule map_genbank_habitats:
	input:
		habitats='corpora/genbank/habitats.txt'
	output:
		mapped_habitats='corpora/genbank/mapped_habitats.txt'
	params:
		plan='plans/map_habitats.plan',
		onto='share/BioNLP-OST+EnovFood-Habitat.obo',
		tomap='share/BioNLP-OST+EnovFood-Habitat.tomap',
		graylist='share/graylist_extended.heads',
		emptywords='share/stopwords_EN.ttg',
		outdir='corpora/genbank',
		outfile='mapped_habitats.txt'
	singularity:config["SINGULARITY_IMG"]
	shell: """rm -rf corpora/genbank/yatea* && alvisnlp -J-Xmx128g -cleanTmp -verbose \
	-alias input {input.habitats} \
    -outputDir {params.outdir} \
	-alias output {params.outfile} \
	-alias ontobiotope {params.onto} \
	-xalias '<ontobiotope-tomap empty-words="{params.emptywords}" graylist="{params.graylist}" whole-proxy-distance="false">{params.tomap}</ontobiotope-tomap>' \
	{params.plan}
	"""


'''
format results
'''
rule format_genbank_results:
	input:
		file='corpora/genbank/GenBank_extraction_full.tsv',
		taxa='corpora/genbank/mapped_taxids.txt',
		habitats='corpora/genbank/mapped_habitats.txt'
	output:
		result='corpora/florilege/genbank/genbank-results.txt'
	conda: 'softwares/envs/obo-utils-env.yaml'
	shell: 'python softwares/scripts/format-genbank-results.py --genbank {input.file} --taxa {input.taxa} --habitats {input.habitats} > {output.result}'




'''---------------------THIS PART TO GET META INFO--------------------------'''


'''
genbank | nb entrees | count_lines(corpora/genbank/GenBank_extraction_full.tsv)
genbank | nb entites | count_lines(corpora/genbank/mapped_taxids.txt)
genbank | nb habitats | count_lines(corpora/genbank/mapped_habitats.txt)
'''
ENTREES_GENBANK = ["genbank/GenBank_extraction_full.tsv"]
SORTIES_GENBANK = ["genbank/taxids.txt", "genbank/habitats.txt" ]
FILES_GENBANK = ENTREES_GENBANK + SORTIES_GENBANK

'''
get stats
'''
rule get_stats_genbank:
        input:
                file="corpora/{file}"
        output:
                stats="corpora/genbank/stats/{file}_stats.csv"
        params:
                c0="source",
                c1="uri",
                c2="valeur",
                c="line"
        run:
                import pandas
                df1 = pandas.read_csv(input.file, names = [params.c], header=None)
                df2 = pandas.DataFrame({params.c0: str(input.file).split("/")[2], params.c1: [input.file], params.c2: [len(df1.index)]})
                df2.to_csv(output.stats, index=False)

'''
merge
'''
rule merge_stats_genbank:
        input:
                files=expand("corpora/genbank/stats/{file}_stats.csv", file=FILES_GENBANK)
        output:
                result="corpora/genbank/stats/stats.full.csv"
        run:
                import pandas
                frames = [ pandas.read_csv(f) for f in input.files ]
                result = pandas.concat(frames)
                result.to_csv(output.result, index=False)


'''
all meta
'''
rule all_meta:
        input:
		"corpora/genbank/stats/stats.full.csv"
