import re

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--pubmed-results', action='store', default='relations.txt', help='PubMed file')

args = parser.parse_args()

unique_mappings = {}
f = open(args.pubmed_results, "r")
for line in f:
    line = line.rstrip()
    fields = line.split("\t")
    if(len(fields) == 15):
        batch = fields[0]
        pmid = fields[1]
        source = fields[2]
        taxid = fields[3]
        surface_taxon = fields[4]
        lemma_taxon = fields[5]
        name = fields[6]
        path = fields[7]
        concept_id = fields[8]
        surface_entity = fields[9]
        lemma_entity = fields[10]
        concept_name = fields[11]
        concept_path = fields[12]
        offset_taxon = fields[13]
        offset_entity = fields[14]
        if(not (batch == "" or pmid == "" or source == "" or taxid == "" or surface_taxon == "" or lemma_taxon == "" 
            or name == "" or path == "" or concept_id == "" or surface_entity == "" or lemma_entity == ""
            or concept_name == "" or concept_path == "")):
            key = taxid+"-"+concept_id
            if key in unique_mappings:
                unique_mappings[key]['entity']['surface'].add(surface_entity)
                unique_mappings[key]['entity']['lemma'].add(lemma_entity)
                unique_mappings[key]['taxon']['surface'].add(surface_taxon)
                unique_mappings[key]['taxon']['lemma'].add(lemma_taxon)
                unique_mappings[key]['pmid'].add(pmid)
            else:
                unique_mappings[key] = {}
                unique_mappings[key]['entity'] = {}
                unique_mappings[key]['taxon'] = {}
                unique_mappings[key]['pmid'] = {pmid}
                unique_mappings[key]['batch'] = batch
                unique_mappings[key]['source'] = source
                unique_mappings[key]['entity']['concept_id'] = concept_id
                unique_mappings[key]['entity']['concept_name'] = concept_name
                unique_mappings[key]['entity']['concept_path'] = concept_path
                unique_mappings[key]['taxon']['taxid'] = taxid
                unique_mappings[key]['taxon']['canonical_name'] = name
                unique_mappings[key]['taxon']['path'] = path
                unique_mappings[key]['entity']['surface'] = {surface_entity}
                unique_mappings[key]['taxon']['surface'] = {surface_taxon}
                unique_mappings[key]['entity']['lemma'] = {lemma_entity}
                unique_mappings[key]['taxon']['lemma'] = {lemma_taxon}
f.close()

for mapping in unique_mappings.values():
   print("\t".join((mapping['batch'],
                         ', '.join(mapping['pmid']),
                         mapping['source'],
                         mapping['taxon']['taxid'],
                         ', '.join(mapping['taxon']['surface']),
                         ', '.join(mapping['taxon']['lemma']),
                         mapping['taxon']['canonical_name'],
                         mapping['taxon']['path'],
                         mapping['entity']['concept_id'],
                         ', '.join(mapping['entity']['surface']),
                         ', '.join(mapping['entity']['lemma']),
                         mapping['entity']['concept_name'],
                         mapping['entity']['concept_path'])
   ))
