import pandas as pd
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--input', action='store', default='corpora/cirm/Levures_2021/Florilege_21012021.xlsx', help='CIRM yeast file')
parser.add_argument('--taxa-index', action='store', default='1', help='index of taxa column')
parser.add_argument('--habitat-index', action='store', default='10,11', help='index(es) of habitat column(s) (comma-delimited if multiple indexes')
parser.add_argument('--taxa-outfile', action='store', default='corpora/cirm/yeast_taxa.txt', help='output list of taxa')
parser.add_argument('--habitat-outfile', action='store', default='corpora/cirm/yeast_habitats.txt', help='output list of habitats')
parser.add_argument('--tsv-outfile', action='store', default='corpora/cirm/Levures_2021/Florilege_21012021.tsv', help='output tab-delimited converted file')

args = parser.parse_args()

taxa_index = int(args.taxa_index)
habitat_indexes = map(int,args.habitat_index.split(','))

cirm_data = pd.read_excel(args.input)

f = open(args.taxa_outfile, 'w')
taxa = cirm_data.iloc[:, taxa_index].dropna().unique()
f.write("\n".join(taxa) + "\n")
f.close()

f = open(args.habitat_outfile, 'w')
for index in habitat_indexes:
    habitats = cirm_data.iloc[:, index].dropna().unique()
    f.write("\n".join(habitats) + "\n")
f.close()

cirm_data.to_csv(args.tsv_outfile, sep="\t", index=False)
