#!/bin/env python3

import logging
import sys
import argparse
import collections


logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S', level=logging.INFO)
Logger = logging.getLogger(__name__)


class BacDiveFormatResult(argparse.ArgumentParser):
    def __init__(self):
        argparse.ArgumentParser.__init__(self, 'format DSMZ extraction results')
        self.add_argument('--bacdive-sample-type', action='store', required=True, dest='bacdive_sample_type', help='')
        self.add_argument('--taxids', action='store', required=True, dest='taxids', help='')
        self.add_argument('--bacdive-to-taxid', action='store', required=True, dest='bacdive_to_taxid', help='')
        self.add_argument('--mapped-habitats', action='store', required=True, dest='mapped_habitats', help='')

    @staticmethod
    def read_entries(filename, *args):
        with open(filename) as f:
            for line in f:
                cols = line.strip().split('\t')
                yield dict(zip(args, cols))

    def _output_lines(self, bacdive_sample_type, bacdive_to_taxid, taxid_microorganisms, mapped_habitats):
        for bd in bacdive_sample_type:
            bacdive_id = bd['bacdive_id']
            if bacdive_id not in bacdive_to_taxid:
                Logger.warning('no taxon for %s' % bacdive_id)
                continue
            taxid = bacdive_to_taxid[bacdive_id]
            if taxid not in taxid_microorganisms:
                Logger.warning('could not find taxon info for %s' % taxid)
                continue
            taxinfo = taxid_microorganisms[taxid]
            habitat = bd['habitat']
            if habitat not in mapped_habitats:
                Logger.warning('no habitat found for %s (%s)' % (bacdive_id, habitat))
                continue
            habinfo = mapped_habitats[habitat]
            yield (
                taxinfo['name'],
                taxinfo['name'],
                taxinfo['taxid'],
                taxinfo['path'],
                habinfo['form'],
                habinfo['obt_id'],
                habinfo['name'],
                habinfo['path'],
                bacdive_id
            )

    def run(self):
        args = self.parse_args()
        bacdive_sample_type = list(BacDiveFormatResult.read_entries(args.bacdive_sample_type, 'bacdive_id', 'habitat'))
        taxid_microorganisms = dict((taxon['taxid'], taxon) for taxon in BacDiveFormatResult.read_entries(args.taxids, 'taxid', 'name', 'path', 'rank'))
        bacdive_to_taxid = dict((bdt['bacdive_id'], bdt['taxid']) for bdt in BacDiveFormatResult.read_entries(args.bacdive_to_taxid, 'bacdive_id', 'taxid'))
        mapped_habitats = dict((mh['text'], mh) for mh in BacDiveFormatResult.read_entries(args.mapped_habitats, 'text', 'form', 'obt_id', 'name', 'path'))
        result_map = collections.defaultdict(list)
        for cols in self._output_lines(bacdive_sample_type, bacdive_to_taxid, taxid_microorganisms, mapped_habitats):
            result_map[(cols[2], cols[5])].append(cols)
        for collist in result_map.values():
            colzip = zip(*collist)
            colpipe = [(',' if i == 8 else '|').join(set(x)) for (i, x) in enumerate(colzip)]
            sys.stdout.write('\t'.join(colpipe))
            sys.stdout.write('\n')


BACDIVE_SAMPLE_TYPE_FILE = 'corpora/dsmz/dsmz-data/sample_type.txt'
TAXID_MICROORGANISMS_FILE = 'ancillaries/extended-microorganisms-taxonomy/taxid_microorganisms.txt'
BACDIVE_TO_TAXID_FILE = 'ancillaries/extended-microorganisms-taxonomy/bacdive-match/bacdive-to-taxid.txt'
MAPPED_HABITATS_FILE = 'corpora/dsmz/mapped_habitats.txt'

if __name__ == '__main__':
    BacDiveFormatResult().run()
