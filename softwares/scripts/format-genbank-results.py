import re

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--genbank', action='store', default='florilege-BD/GenBank/req1_sup800_bacteria-descriptors_clean.csv', help='GenBank file')
parser.add_argument('--taxa', action='store', default='florilege-BD/GenBank/mapped_taxids.txt', help='mapped taxa file')
parser.add_argument('--habitats', action='store', default='florilege-BD/GenBank/mapped_habitats.txt', help='mapped habitat file')

args = parser.parse_args()

genbank_file = args.genbank
taxa_file = args.taxa
habitat_file = args.habitats 

habitat_dict = {}
hf = open(habitat_file, "r")
for line in hf:
    line = line.rstrip()
    p = re.compile(r'([^\t]+)\t(\S.+)')
    m = p.match(line)
    if(m):
        habitat = m.group(1)
        habitat_info = m.group(2)
        if habitat in habitat_dict:
            habitat_dict[habitat].add(habitat_info)
        else:
            habitat_dict[habitat] = {habitat_info}
hf.close()

taxa_dict = {}
tf = open(taxa_file, "r")
for line in tf:
    line = line.rstrip()
    p = re.compile(r'([^\t]+)\t(\S.+)')
    m = p.match(line)
    if(m):
        taxon = m.group(1)
        taxon_info = m.group(2)
        taxa_dict[taxon] = taxon_info
tf.close()

def add_entry(mappings, habitat, concepts, taxid, taxon, name, path, accession):
    for concept in concepts:
        surface_form, concept_id, concept_name, concept_path = concept.split('\t')
        key = taxid+"-"+concept_id
        if key in mappings:
            mappings[key]['habitat']['surface'].add(surface_form)
            mappings[key]['taxon']['surface'].add(taxon)
            mappings[key]['accession'].add(accession)
        else:
            mappings[key] = {}
            mappings[key]['habitat'] = {}
            mappings[key]['taxon'] = {}
            mappings[key]['accession'] = {accession}
            mappings[key]['habitat']['concept_id'] = concept_id
            mappings[key]['habitat']['concept_name'] = concept_name
            mappings[key]['habitat']['concept_path'] = concept_path
            mappings[key]['taxon']['taxid'] = taxid
            mappings[key]['taxon']['canonical_name'] = name
            mappings[key]['taxon']['path'] = path
            mappings[key]['habitat']['surface'] = {surface_form}
            mappings[key]['taxon']['surface'] = {taxon}
    return mappings

unique_mappings = {}
gf = open(genbank_file, "r")
for num, line in enumerate(gf, 1):
    if (num > 2):
        line = line.rstrip("\n")
        fields = line.split("\t")
        taxon = re.sub(r'\s+', ' ', fields[2]).strip()
        habitat = re.sub(r'\s+', ' ', fields[6]).strip()
        host = re.sub(r'\s+', ' ', fields[7]).strip()
        accession = re.sub(r'\s+', ' ', fields[0]).strip()
        taxid = re.sub(r'\s+', ' ', fields[4]).strip()
        if taxid in taxa_dict:
            name, taxid2, path = taxa_dict[taxid].split("\t")
            if habitat in habitat_dict:
                concepts = habitat_dict[habitat]
                unique_mappings = add_entry(unique_mappings, habitat, concepts, taxid2, taxon, name, path, accession)
            if host in habitat_dict:
                concepts = habitat_dict[host]
                unique_mappings = add_entry(unique_mappings, host, concepts, taxid2, taxon, name, path, accession)
gf.close()

for mapping in unique_mappings.values():
   print("\t".join(('|'.join(mapping['taxon']['surface']),
             mapping['taxon']['canonical_name'],
             mapping['taxon']['taxid'],
             mapping['taxon']['path'],
             '|'.join(mapping['habitat']['surface']),
             mapping['habitat']['concept_id'],
             mapping['habitat']['concept_name'],
             mapping['habitat']['concept_path'],
             ','.join(mapping['accession']))
   ))
