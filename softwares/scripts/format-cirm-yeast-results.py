import re
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--cirm', action='store', default='corpora/cirm/', help='CIRM yeast file')
parser.add_argument('--taxa', action='store', default='corpora/cirm/mapped_taxa.txt', help='mapped taxa file')
parser.add_argument('--habitats', action='store', default='corpora/cirm/mapped_habitats.txt', help='mapped habitat file')
parser.add_argument('--taxa-index', action='store', default='1', help='index of taxa column')
parser.add_argument('--habitat-index', action='store', default='10,11', help='index(es) of habitat column(s) (comma-delimited if multiple indexes')

args = parser.parse_args()

cirm_file = args.cirm
taxa_file = args.taxa
habitat_file = args.habitats
taxa_index = int(args.taxa_index)
habitat_indexes = list(map(int,args.habitat_index.split(',')))

habitat_dict = {}
hf = open(habitat_file, "r")
for line in hf:
    line = line.rstrip()
    p = re.compile(r'([^\t]+)\t(\S.+)')
    m = p.match(line)
    if(m):
        habitat = m.group(1)
        habitat_info = m.group(2)
        if habitat in habitat_dict:
            habitat_dict[habitat].add(habitat_info)
        else:
            habitat_dict[habitat] = {habitat_info}
hf.close()

taxa_dict = {}
tf = open(taxa_file, "r")
for line in tf:
    line = line.rstrip()
    p = re.compile(r'([^\t]+)\t(\S.+)')
    m = p.match(line)
    if(m):
        taxon = m.group(1)
        taxon_info = m.group(2)
        taxa_dict[taxon] = taxon_info
tf.close()

def add_entry(mappings, habitat, concepts, taxid, taxon, name, path):
    for concept in concepts:
        surface_form, concept_id, concept_name, concept_path = concept.split('\t')
        key = taxid+"-"+concept_id
        if key in mappings:
            mappings[key]['habitat']['surface'].add(habitat)
            mappings[key]['taxon']['surface'].add(taxon)
        else:
            mappings[key] = {}
            mappings[key]['habitat'] = {}
            mappings[key]['taxon'] = {}
            mappings[key]['habitat']['concept_id'] = concept_id
            mappings[key]['habitat']['concept_name'] = concept_name
            mappings[key]['habitat']['concept_path'] = concept_path
            mappings[key]['taxon']['taxid'] = taxid
            mappings[key]['taxon']['canonical_name'] = name
            mappings[key]['taxon']['path'] = path
            mappings[key]['habitat']['surface'] = {habitat}
            mappings[key]['taxon']['surface'] = {taxon}
    return mappings

unique_mappings = {}
cf = open(cirm_file, "r")
for num, line in enumerate(cf, 1):
    if (num > 1):
        line = line.rstrip("\n")
        fields = line.split("\t")
        taxon = re.sub(r'\s+', ' ', fields[taxa_index]).strip()
        if taxon in taxa_dict:
            taxid, name, path = taxa_dict[taxon].split("\t")
            for index in habitat_indexes:
                habitat = re.sub(r'\s+', ' ', fields[index]).strip()
                if habitat in habitat_dict:
                    concepts = habitat_dict[habitat]
                    unique_mappings = add_entry(unique_mappings, habitat, concepts, taxid, taxon, name, path)
    
cf.close()

for mapping in unique_mappings.values():
    print("\t".join(('|'.join(mapping['taxon']['surface']),
              mapping['taxon']['canonical_name'],
              mapping['taxon']['taxid'],
              mapping['taxon']['path'],
              '|'.join(mapping['habitat']['surface']),
              mapping['habitat']['concept_id'],
              mapping['habitat']['concept_name'],
              mapping['habitat']['concept_path'])
    ))

