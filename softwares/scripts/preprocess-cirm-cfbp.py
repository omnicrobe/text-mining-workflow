import pandas as pd
import argparse
import re

parser = argparse.ArgumentParser()
parser.add_argument('--input', action='store', default='corpora/cirm/CFBP_2020/CFPB_22_sept_2020_Type.xlsx', help='CIRM file')
parser.add_argument('--taxa-index', action='store', default='1', help='index of taxa column')
parser.add_argument('--strain-index', action='store', default='0', help='index of strain column')
parser.add_argument('--habitat-index', action='store', default='9,10,14,23', help='index(es) of habitat column(s) (comma-delimited if multiple indexes')
parser.add_argument('--taxa-outfile', action='store', default='corpora/cirm/cfbp_taxa.txt', help='output list of taxa')
parser.add_argument('--habitat-outfile', action='store', default='corpora/cirm/cfbp_habitats.txt', help='output list of habitats')
parser.add_argument('--tsv-outfile', action='store', default='corpora/cirm/CFBP_2020/CFPB_22_sept_2020_Type.tsv', help='output tab-delimited converted file')

args = parser.parse_args()

taxa_index = int(args.taxa_index)
strain_index = int(args.strain_index)
habitat_indexes = map(int,args.habitat_index.split(','))

cirm_data = pd.read_excel(args.input, dtype='object')
cirm_data.replace(to_replace=[r"\\n|\\r", "\n|\r"], value=[" "," "], regex=True, inplace=True)
cirm_data.replace(to_replace=[r'["“”]'], value=[""], regex=True, inplace=True)

taxa_dict = set()
for i in range(len(cirm_data)) :
  taxon = str(cirm_data.iloc[i, taxa_index])
  strain = str(cirm_data.iloc[i, strain_index])
  taxa_dict.add(taxon)
  taxa_dict.add(taxon + " CFBP " + strain)
  taxa_dict.add(taxon + " CFBP" + strain)
  taxa_dict.add(taxon + " CFBP:" + strain)
  taxon2 = re.sub(r'( [0-9\.]+)$', r'', taxon)
  taxa_dict.add(taxon2)
  taxa_dict.add(taxon2 + " CFBP " + strain)
  taxa_dict.add(taxon2 + " CFBP" + strain)
  taxa_dict.add(taxon2 + " CFBP:" + strain)
  taxa_dict.add("CFBP " + strain)
  taxa_dict.add("CFBP" + strain)
  taxa_dict.add("CFBP:" + strain)

f = open(args.taxa_outfile, 'w')
f.write("\n".join(taxa_dict) + "\n")
f.close()

f = open(args.habitat_outfile, 'w')
for index in habitat_indexes:
    habitats = cirm_data.iloc[:, index].dropna().unique()
    for habitat in habitats:
      f.write(re.sub(r'\.$', r'', habitat) + "\n")
f.close()

cirm_data.to_csv(args.tsv_outfile, sep="\t", index=False)
