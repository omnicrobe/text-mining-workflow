import pandas as pd
import argparse
import re

parser = argparse.ArgumentParser()
parser.add_argument('--input', action='store', default='corpora/cirm/BIA_2021/florilege_export_final_17_02_21.xlsx', help='CIRM file')
parser.add_argument('--taxa-index', action='store', default='2', help='index of taxa column')
parser.add_argument('--strain-index', action='store', default='1', help='index of strain column')
parser.add_argument('--habitat-index', action='store', default='15', help='index(es) of habitat column(s) (comma-delimited if multiple indexes')
parser.add_argument('--taxa-outfile', action='store', default='corpora/cirm/taxa.txt', help='output list of taxa')
parser.add_argument('--habitat-outfile', action='store', default='corpora/cirm/habitats.txt', help='output list of habitats')
parser.add_argument('--tsv-outfile', action='store', default='corpora/cirm/BIA_2021/florilege_export_final_17_02_21.tsv', help='output tab-delimited converted file')

args = parser.parse_args()

taxa_index = int(args.taxa_index)
strain_index = int(args.strain_index)
habitat_indexes = map(int,args.habitat_index.split(','))

cirm_data = pd.read_excel(args.input)

taxa_dict = set()
for i in range(len(cirm_data)) :
  taxon = cirm_data.iloc[i, taxa_index]
  strain = cirm_data.iloc[i, strain_index]
  taxa_dict.add(taxon)
  taxa_dict.add(taxon + " " + strain)
  taxa_dict.add(taxon + " " + re.sub(r'([0-9]+)$', r' \1', strain))

f = open(args.taxa_outfile, 'w')
f.write("\n".join(taxa_dict) + "\n")
f.close()

f = open(args.habitat_outfile, 'w')
for index in habitat_indexes:
    habitats = cirm_data.iloc[:, index].dropna().unique()
    f.write("\n".join(habitats) + "\n")
f.close()

cirm_data.to_csv(args.tsv_outfile, sep="\t", index=False)
