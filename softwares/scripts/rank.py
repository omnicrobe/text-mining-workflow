# Reads a TSV file, counts occurrences of each identifier and ranks them by count
#
# Usage:
#    python rank.py COLUMN_ID COLUMN_LABEL
#
# COLUMN_ID: column of identifiers (first is 0)
# COLUMN_LABEL: column of the labels (first is 0)
#
# To count concepts, set COLUMN_ID to the concept identifier (ncbi:000000 or OBT:000000).
# To count surface forms, set COLUMN_ID to the surface forms.
# Set COLUMN_LABEL to the canonical form.
#
# Output is a TSV. Columns: rank, identifier, label, count.
# Two items with the same count have the same rank.

import sys
import operator

COLUMN_ID = int(sys.argv[1])
COLUMN_LABEL = int(sys.argv[2])


class Item:
    def __init__(self, iid, label):
        self.iid = iid
        self.label = label
        self.count = 1


ITEMS = {}
for line in sys.stdin:
    cols = line.strip().split('\t')
    if len(cols) <= COLUMN_ID:
        continue
    iid = cols[COLUMN_ID]
    if iid in ITEMS:
        ITEMS[iid].count += 1
    else:
        label = cols[COLUMN_LABEL]
        ITEMS[iid] = Item(iid, label)

RANKED = sorted(ITEMS.values(), reverse=True, key=operator.attrgetter('count'))

prev_rank = 0
prev_count = sys.maxsize
for n, i in enumerate(RANKED):
    if i.count == prev_count:
        rank = prev_rank
    else:
        rank = prev_rank + 1
        prev_rank = rank
        prev_count = i.count
    print('\t'.join((str(rank), i.iid, i.label, str(i.count))))
