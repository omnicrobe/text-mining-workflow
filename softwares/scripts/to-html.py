import os
import pandas
import csv
import json


def get_score(jfile, entityList = ["Standard scoring"], corpus = "BB" , task = "BB-norm"):
    dict = {"Corpus":[], "Task":[], "Pairing":[], "Score Scope":[], "Score Name": [], "Value": []}
    data = json.load(open(jfile))
    for d in (data['evaluation'])['global-evaluations']:
        scorings = d['scorings']
        for s in scorings:
            if s['name'] in entityList:
                for m in s['measures']:
                    dict["Corpus"].append(corpus)
                    dict["Task"].append(task)
                    dict["Pairing"].append(d['name'])
                    dict["Score Scope"].append(s['name'])
                    dict["Score Name"].append(m['name'])
                    dict["Value"].append(m['value'])
    return dict


def get_score_norm(jfile):
    return get_score(jfile, entityList = ["Standard scoring", "Habitats", "Phenotypes", "Microorganisms"], corpus = "BB19" , task = "BB-norm")

def get_score_kb(jfile):
    return get_score(jfile, entityList = ["Standard scoring"], corpus = "BB" , task = "BB-kb")

def get_score_rel(jfile):
    return get_score(jfile, entityList = ["Standard scoring", "Lives_In and Exhibits", "Lives_In", "Exhibits"], corpus = "BB" , task = "BB-rel")

# Main
if __name__ == '__main__':

    import sys
    
    from optparse import OptionParser, OptionGroup

    optparser = OptionParser(usage="%extract data from genbank bank")
    group = OptionGroup(optparser, "Main Options", "")
    optparser.add_option("-f", "--file", default="None", dest="eval_file", help="eval file (compatible json)", metavar="FILE")
    optparser.add_option("-c", "--corpus", default="BB19", dest="corpus", help="eval corpus")
    optparser.add_option("-t", "--task", default="None", dest="task", help="eval task")
    optparser.add_option("-o", "--output", default="None", dest="html", help="generated html", metavar="FILE")

    (options, args) = optparser.parse_args()

    task = options.task
    jfile = options.eval_file

    if task == "BB-norm":
        dict = get_score(jfile, entityList = ["Standard scoring", "Habitats", "Phenotypes", "Microorganisms"], corpus = "BB19" , task = "BB-norm")

    if task == "BB-rel":
        dict =  get_score(jfile, entityList = ["Standard scoring", "Lives_In and Exhibits", "Lives_In", "Exhibits"], corpus = "BB" , task = "BB-rel")

    if task == "BB-kb":
        dict = get_score(jfile, entityList = ["Standard scoring"], corpus = "BB" , task = "BB-kb")
    
    df = pandas.DataFrame(dict)

    html = df.to_html()
  
    # write html to file
    text_file = open(options.html, "w")
    text_file.write(html)
    text_file.close()

