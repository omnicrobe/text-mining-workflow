# Compare a rank file to the previous rank file.
#
# Usage:
#    python.py compare-ranks.py RANK_OLD RANK_NEW
#
# RANK_OLD: older rank file as produced by rank.py
# RANK_NEW: newer rank file as produced by rank.py
#
# Output is a TSV containing the newer ranks with additional columns: rank, rank diff, identifier, label, count, count diff, old label (if label has changed)

import sys
import operator

RANKFILE_PREV = sys.argv[1]
RANKFILE_NEXT = sys.argv[2]


class Item:
    def __init__(self, iid, label, rank, count):
        self.iid = iid
        self.label = label
        self.rank = rank
        self.count = count


def read_ranks(filename):
    result = {}
    with open(filename) as f:
        for line in f:
            rank, iid, label, count = line.strip().split('\t')
            result[iid] = Item(iid, label, int(rank), int(count))
    return result


RANK_PREV = read_ranks(RANKFILE_PREV)
RANK_NEXT = read_ranks(RANKFILE_NEXT)

maxrank = max(i.rank for i in RANK_NEXT.values())
for next_i in RANK_NEXT.values():
    if next_i.iid in RANK_PREV:
        prev_i = RANK_PREV[next_i.iid]
        next_i.diff_rank = prev_i.rank - next_i.rank
        next_i.diff_count = next_i.count - prev_i.count
        if prev_i.label != next_i.label:
            next_i.diff_label = prev_i.label
        else:
            next_i.diff_label = ''
    else:
        next_i.diff_rank = 1 + maxrank - next_i.rank
        next_i.diff_count = next_i.count
        next_i.diff_label = ''

RANKED = sorted(RANK_NEXT.values(), key=operator.attrgetter('rank'))
print('\t'.join(('rank', 'diff', 'id', 'label', 'count', 'count diff', 'old label')))
for i in RANKED:
    print('\t'.join((str(i.rank), '%+d' % i.diff_rank, i.iid, i.label, str(i.count), '%+d' % i.diff_count, i.diff_label)))
