#!/bin/env python3

import logging
import sys
import json
import os


logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S', level=logging.INFO)
Logger = logging.getLogger(__name__)


BACDIVE_ENTRIES = sys.argv[1]


COUNT = {
    'files': 0,
    'entries': 0,
    'entries without isolation': 0,
    'isolations': 0
}
KEYS = set()
Logger.info('reading BacDive entries in directory: %s' % BACDIVE_ENTRIES)
for dirpath, _, filenames in os.walk(BACDIVE_ENTRIES):
    for fn in filenames:
        if not fn.endswith('.json'):
            continue
        with open(os.path.join(dirpath, fn)) as f:
            COUNT['files'] += 1
            j = json.load(f)
            res = j['results']
            if res:
                for entry in res.values():
                    bacdive_id = str(entry['General']['BacDive-ID'])
                    COUNT['entries'] += 1
                    try:
                        isolation = entry['Isolation, sampling and environmental information']['isolation']
                        if isinstance(isolation, dict):
                            isolation = [isolation]
                        for iso in isolation:
                            KEYS |= set(iso.keys())
                            sample_type = ' '.join(iso['sample type'].split())
                            sys.stdout.write('%s\t%s\n' % (bacdive_id, sample_type))
                            COUNT['isolations'] += 1
                    except KeyError:
                        COUNT['entries without isolation'] += 1
for k, v in COUNT.items():
    Logger.info('%s: %d' % (k, v))
Logger.info('isolation keys: %s' % str(KEYS))
