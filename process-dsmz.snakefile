## Processing data sources DSMZ

configfile: 'config/config.yaml'

'''
## variables, check values into config file
_ontobiotope = config['ONTOBIOTOPE']
_names = config['NCBI_TAXO_NAMES']
_pubmed_batches_home = config['PUBMED_BATCHES_HOME']
_pubmed_xslt_file = config['PUBMED_XSLT_FILE']
_ncbi_taxo_microorganisms = config['NCBI_TAXO_MICROORGANISMS']
_ncbi_taxo_id = config['NCBI_TAXO_ID']
_ncbi_taxo_and_id_microorganisms = config['NCBI_TAXO_AND_ID_MICROORGANISMS']
_genbank_db = config['GENBANK_DB']
_dsmz_habitat_corpus = config['DSMZ_HABITAT_CORPUS']
_map_bacdive_taxid = config['MAP_BACDIVE_TAXID']
_bacdiva_entries = config['BACDIVE_ENTRIES']
'''

''' all
'''
rule all:
    input:
        'corpora/florilege/dsmz/dsmz-results.txt'


'''
extract dsmz sample
'''
rule extract_sample_type:
    input:
        bacdive_entries='share/extended-microorganisms-taxonomy/bacdive-strains/entries'
    output:
        'corpora/dsmz/dsmz-data/sample_type.txt'
    shell: '''softwares/scripts/bacdive-extract-isolation.py {input.bacdive_entries} >{output}'''


'''
get habitats
'''
rule get_dsmz_habitat:
    input:
        'corpora/dsmz/dsmz-data/sample_type.txt'

    output:
        'corpora/dsmz/habitats.txt'

    shell: '''cut -f2 {input} |sort -u > {output}'''


'''
map habitats of microorganisms
'''
rule map_dsmz_habitats:
    input:
        'corpora/dsmz/habitats.txt'

    output:
        'corpora/dsmz/mapped_habitats.txt',
        yatea_candidates='corpora/dsmz/yatea/candidates.xml',
        yatea_var_candidates='corpora/dsmz/yatea-var/candidates.xml'

    params:
        plan='plans/map_habitats.plan',
        onto='share/BioNLP-OST+EnovFood-Habitat.obo',
        tomap='share/BioNLP-OST+EnovFood-Habitat.tomap',
        graylist='share/graylist_extended.heads',
        emptywords='share/stopwords_EN.ttg',
        outdir='corpora/dsmz',
        outfile='mapped_habitats.txt'

    singularity:
        config["SINGULARITY_IMG"]

    shell: '''alvisnlp -J-Xmx32g -cleanTmp -verbose \
    -alias input {input} \
    -outputDir {params.outdir} \
    -alias output {params.outfile} \
    -alias ontobiotope {params.onto} \
    -xalias '<ontobiotope-tomap empty-words="{params.emptywords}" graylist="{params.graylist}" whole-proxy-distance="false">{params.tomap}</ontobiotope-tomap>' \
    {params.plan}'''


'''
format results
'''
rule format_dsmz_results:
    input:
        script='softwares/scripts/bacdive-format-results.py',
        bacdive_sample_type='corpora/dsmz/dsmz-data/sample_type.txt',
        mapped_habitats='corpora/dsmz/mapped_habitats.txt',
        taxids='share/extended-microorganisms-taxonomy/taxid_microorganisms.txt',
        bacdive_to_taxid='share/extended-microorganisms-taxonomy/bacdive-match/bacdive-to-taxid.txt'

    output:
        result='corpora/florilege/dsmz/dsmz-results.txt',
        logfile='corpora/florilege/dsmz/dsmz-results.log'

    conda:
        'softwares/envs/obo-utils-env.yaml'

    shell:
        '''{input.script} --bacdive-sample-type {input.bacdive_sample_type} --taxids {input.taxids} --bacdive-to-taxid {input.bacdive_to_taxid} --mapped-habitats {input.mapped_habitats} >{output.result} 2>{output.logfile}'''






'''--------------------THIS PART TO GET META INFO--------------------------------'''

'''
dsmz | nb entrees | count_lines(corpora/dsmz/dsmz-data/sample_type.txt)
dsmz | nb habitats | count_lines(corpora/dsmz/mapped_habitats.txt)
'''
ENTREES_DSMZ = ["dsmz/dsmz-data/sample_type.txt"]
SORTIES_DSMZ = ["dsmz/mapped_habitats.txt" ]
FILES_DSMZ = ENTREES_DSMZ + SORTIES_DSMZ

'''
get stats
'''
rule stats_dsmz:
        input:
                file="corpora/{file}"
        output:
                stats="corpora/dsmz/stats/{file}_stats.csv"
        params:
                c0="source",
                c1="uri",
                c2="valeur",
                c="line"
        run:
                import pandas
                df1 = pandas.read_csv(input.file, names = [params.c], header=None)
                df2 = pandas.DataFrame({params.c0: str(input.file).split("/")[2], params.c1: [str(input.file)], params.c2: [len(df1.index)]})
                df2.to_csv(output.stats, index=False)

'''
merge
'''
rule merge_stats_dsmz:
        input:
                files=expand("corpora/dsmz/stats/{file}_stats.csv", file=FILES_DSMZ)
        output:
                result="corpora/dsmz/stats/stats.full.csv"
        run:
                import pandas
                frames = [ pandas.read_csv(f) for f in input.files ]
                result = pandas.concat(frames)
                result.to_csv(output.result, index=False)


'''
all meta
'''
rule all_meta:
        input:
		"corpora/dsmz/stats/stats.full.csv"
