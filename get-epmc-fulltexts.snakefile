## config file
configfile: "config/config.yaml"


''' 
all
'''
rule all:
    input:
        pmcid_file = "corpora/epmc/pmcids.full.txt",
        meta = "corpora/pubmed/info.csv"
		

BATCHES, = glob_wildcards("corpora/pubmed/batches/{id}/pmids.txt")
'''
get pmcids for each batch
'''
rule get_PMCID:
    input:
        corpus = "corpora/pubmed/batches",
        pmid_file = "corpora/pubmed/batches/{B}/pmids.txt"
    output:
        pmcid_file = "corpora/epmc/pmcids/{B}/pmcids.txt"
    params:
        request_root = config["PMC_REQUEST_ROOT"]
    run:
        import io
        import os
        import requests
        from requests.structures import CaseInsensitiveDict
        from requests.utils import requote_uri
        import pandas
        import json
        df = pandas.read_csv(input.pmid_file, names = ['pmid'])
        RQT_OPS = '%20OR%20EXT_ID:'.join(list(map(str, df.pmid)))
        request = params.request_root + "search?query=(" + RQT_OPS + ")"
        request = request + "%20AND%20SRC:MED%20AND%20OPEN_ACCESS:y&resultType=idlist"
        headers = CaseInsensitiveDict()
        headers["accept"] = "application/json"
        resp = requests.get(request, headers=headers)
        binary = resp.content
        Jdata = json.loads(binary)
        Jlist  = Jdata["resultList"]["result"]
        pmcids = [e['pmcid'] for e in Jlist if 'pmcid' in e.keys()]
        pandas.DataFrame(pmcids).to_csv(output.pmcid_file, index=False)
        
        
'''
get fulltexts for each batch
'''
rule get_XML:
    input:
        pmcid_file = "corpora/epmc/pmcids/{B}/pmcids.txt"
    output:
        corpus_folder = directory("corpora/epmc/batches/{B}/")
    params:
        request_root = config["PMC_REQUEST_ROOT"]
    run:
        import os
        import io
        import requests
        from requests.structures import CaseInsensitiveDict
        from requests.utils import requote_uri
        import pandas
        import json
        df = pandas.read_csv(input.pmcid_file, names = ['pmcid'])
        data = [] 
        for index, row in df.iterrows():
            pmcid = row["pmcid"]
            request = params.request_root
            request = request + pmcid
            request = request + "/fullTextXML"
            headers = CaseInsensitiveDict()
            headers["accept"] = "application/xml"
            resp = requests.get(request, headers=headers)
            filename = output.corpus_folder + '/'+ pmcid + '.xml'
            os.makedirs(os.path.dirname(filename), exist_ok=True)
            with open(filename, 'w+') as f:
                f.write(resp.text)


'''
'''
rule merge:
    input:
        batch_folder=expand("corpora/epmc/pmcids/{B}/", B=BATCHES)
    output:
        pmcid_file="corpora/epmc/pmcids.full.txt"
    params:
        pmcid_file = "pmcids.txt"
    run:
        pf = input.batch_folder + params.pmcid_file
        with open(output.pmcid_file, 'w') as out:
            for fname in pf:
                with open(fname) as infile:
                    out.write(infile.read())



'''
get metadata

rule get_meta:
	input:
		corpora="corpora/empc/batches"
	output:
		meta="corpora/empc/info.csv"
	run:
		import datetime
		import pandas
		date = datetime.datetime.now().strftime("%Y%m%d")
		batches, = glob_wildcards(input.corpora + "/{id}/batch.xml")
		corpus_size = len(batches)
		data = {"source": "EPMC", 
			"name": ["microbio"], 
			"size (nb. batches x 1000 abstracts)" : [corpus_size], 
			"date" : [date]}
		df = pandas.DataFrame(data)
		df.to_csv(output.meta, index=False)
'''