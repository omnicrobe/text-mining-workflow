<?xml version="1.0" encoding="UTF-8"?>

<alvisnlp-plan id="ontology-analyzer">

  <param name="input">
    <alias module="read-obo" param="oboFiles"/>
  </param>

  <param name="output">
    <alias module="print-process" param="corpusFile"/>
  </param>

  <read-obo class="OBOReader">
    <oboFiles>share/OntoBiotope-v53zb-Habitat.obo</oboFiles>
    <pathFeature>path</pathFeature>
  </read-obo>

  <words class="WoSMig"/>
  <sentences class="SeSMig"/>

  <genia class="GeniaTagger">    
    <posFeature>pos</posFeature>
    <lemmaFeature>lemma</lemmaFeature>
    <treeTaggerTagset>true</treeTaggerTagset>
  </genia>

  <adapt-hyphens class="Action">
    <target>documents.sections.layer:words[@pos == "HYPH"]</target>
    <action>set:feat:pos(str:seds(@pos,"HYPH",":"))</action>
    <setFeatures/>
  </adapt-hyphens>

    <adapt-affix class="Action">
    <target>documents.sections.layer:words[@pos =~ "AFX"]</target>
    <action>set:feat:pos("NN")</action>
    <setFeatures/>
    </adapt-affix>

    <syno class="PatternMatcher">
      <pattern>
	(group1: [not @lemma == "commodity"]+)
	(([@lemma == "primary"][@lemma == "commodity"])|
	(([@lemma == "commodity"][@lemma == "and"])?[@lemma == "food"]))
      </pattern>
      <actions>
	<createAnnotation group="group1" layer="process"/>
      </actions>
    </syno>

    <POS-tag-correction-1 class="Action">
      <target>documents.sections.layer:words[(@pos == "VVG" or @pos == "VVN" or @pos == "VVD") and before:words{-1}.@form == "-"]</target>
      <action>set:feat:lemma(str:lower(@form))</action>
      <setFeatures/>
    </POS-tag-correction-1>
    <POS-tag-correction class="Action">
      <target>documents.sections.layer:words[(@pos == "VVG" or @pos == "VVN" or @pos == "VVD") and before:words{-1}.@form == "-"]</target>
      <action>set:feat:pos("JJ")</action>
      <setFeatures/>
    </POS-tag-correction>

    <lemma-correction class="Action">
      <target>documents.sections.layer:words[(@pos == "NN" or @pos == "JJ") and @form =~ "ing$" ]</target>
      <action>set:feat:lemma(str:lower(@form))</action>
      <setFeatures/>
    </lemma-correction>
    <lemma-correction-2 class="Action">
      <target>documents.sections.layer:words[@pos == "JJ" and @form =~ "ed$" ]</target>
      <action>set:feat:lemma(str:lower(@form))</action>
      <setFeatures/>
    </lemma-correction-2>

    <specific-POS-tag-correction class="Action">
      <target>documents.sections.layer:words[@form == "wrt"]</target>
      <action>set:feat:pos("IN")</action>
      <setFeatures/>
    </specific-POS-tag-correction>

    <POS-tag-correction2-1 class="Action">
      <target>documents.sections.layer:words[@pos == "VVN" or @pos == "VVD"]</target>
      <action>set:feat:lemma(@form)</action>
      <setFeatures/>
    </POS-tag-correction2-1>
    <POS-tag-correction2 class="Action">
      <target>documents.sections.layer:words[(@pos == "VVN" or @pos == "VVD") and (@form =~ "ed$" or not end == outside:sentences.end)]</target>
      <action>set:feat:pos("JJ")</action>
      <setFeatures/>
    </POS-tag-correction2>
    <POS-tag-correction2-2 class="Action">
      <target>documents.sections.layer:words[@pos == "VVN" or @pos == "VVD"]</target>
      <action>set:feat:pos("NN")</action>
      <setFeatures/>
    </POS-tag-correction2-2>

    <POS-tag-correction5-1 class="Action">
      <target>documents.sections.layer:words[@pos == "VVG"]</target>
      <action>set:feat:lemma(@form)</action>
      <setFeatures/>
    </POS-tag-correction5-1>

    <POS-tag-correction5-2 class="Action">
      <target>documents.sections.layer:words[@pos == "VVG" and end == outside:sentences.end]</target>
      <action>set:feat:pos("NN")</action>
      <setFeatures/>
    </POS-tag-correction5-2>

    <POS-tag-correction3-1 class="Action">
      <target>documents.sections.layer:words[@pos == "VV" or @pos == "VVP"]</target>
      <action>set:feat:lemma(@form)</action>
      <setFeatures/>
    </POS-tag-correction3-1>
    
    <POS-tag-correction3 class="Action">
      <target>documents.sections.layer:words[@pos == "VV" or @pos == "VVP"]</target>
      <action>set:feat:pos("NN")</action>
      <setFeatures/>
    </POS-tag-correction3>

    <POS-tag-correction4 class="Action">
      <target>documents.sections.layer:words[@pos == "VVZ"]</target>
      <action>set:feat:pos("NNS")</action>
      <setFeatures/>
    </POS-tag-correction4>

    <print-process class="TabularExport">
      <outDir>.</outDir>
      <corpusFile>"food-process-lexicon.txt"</corpusFile>
      <lines>documents[nav:features:path[@value =~ "/OBT:000397/"]].sections.layer:process</lines>
      <columns separator=";">
    	@form;
	str:join:' '(inside:words,@form);
	str:join:' '(inside:words,@lemma);
    	section.document.@id
      </columns>
    </print-process>


</alvisnlp-plan>
