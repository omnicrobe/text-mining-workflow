/* ncbi, dsmz, gb et cirm ids equivalence table */

DROP TABLE IF EXISTS taxon_ids_map;

CREATE TABLE IF NOT EXISTS taxon_ids_map
(
 id serial unique not null,
 ncbi varchar(50),
 dsmz varchar(50),
 gb varchar(50),
 cirm varchar(50),
 primary key (id)
);


/* DROITS */
GRANT SELECT, DELETE, INSERT, UPDATE ON TABLE taxon_ids_map TO flo_admin;
GRANT SELECT, DELETE, INSERT, UPDATE ON TABLE taxon_ids_map TO flo_admin_bis;
GRANT SELECT ON TABLE taxon_ids_map TO flo_user;


/* INDEX */


/* import equivalence table (csv) using */
COPY taxon_ids_map(ncbi,dsmz,gb,cirm) 
FROM 'C:\tmp\equivalence-table.csv' DELIMITER ',' CSV HEADER;


