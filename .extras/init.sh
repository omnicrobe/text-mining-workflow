#!/bin/bash

DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC

apt-get -y update
apt-get -y install maven 
apt-get -y install git 
#apt-get -y install expect 
apt-get -y install wget 
apt-get -y install xmlstarlet 
apt-get -y install zip 
apt-get -y install make 
apt-get -y install ruby 
apt-get -y install g++ 
apt-get -y install gcc 
apt-get -y install libboost-all-dev 
apt-get -y install flex 
apt-get -y install openjdk-8-jdk && echo oracle-java7-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections 
apt-get -y clean
 
export VERSION=1.17.5 OS=linux ARCH=amd64 && wget -O /tmp/go${VERSION}.${OS}-${ARCH}.tar.gz https://dl.google.com/go/go${VERSION}.${OS}-${ARCH}.tar.gz && tar -C / -xzf /tmp/go${VERSION}.${OS}-${ARCH}.tar.gz && echo 'export PATH=$PATH:/go/bin' >> ~/.bashrc && source ~/.bashrc

wget https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b /go/bin && echo 'export PATH=$PATH:/go/bin' >> ~/.bashrc && PATH=$PATH:/go/bin && git clone https://github.com/sylabs/singularity.git && cd singularity && git checkout v3.9.2 && ./mconfig && make -C builddir && make -C ./builddir install
