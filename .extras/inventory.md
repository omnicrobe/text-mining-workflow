# plan entities.plan
This plan is defined to generate the text mining results for the Florilege database.

The main plan is entities.plan which incules several sub-plans :


## plans

* entities.plan
	* taxa.plan
		* strains-1.plan
	* syntax.plan
		* segmentation.plan
			* number-and-dates.plan
	* strains-2.plan
	* pos-tag-lemma-postprocessing.plan
	* tomap-habitats.plan
		* tag-food-derivative.plan
		* tag-food-process.plan
	* tomap-microbial-phenotypes.plan
	* use-extraction.plan
	* anaphora.plan

## resources

### data

* entities.plan
	* resources/2017MeshTree.txt
	* resources/stopwords_EN.txt
	* resources/microorganism_stopwords.txt
	* /projet/mig/save/textemig/projet-work/resources/stanford-ner-2014-06-16/classifiers/english.all.3class.distsim.crf.ser.gz
	* /projet/mig/save/textemig/projet-work/resources/ncbi-taxonomy/taxid_microorganisms.txt
	* /projet/maiage/save/textemig/projet-work/software/Ab3P-v1.5
	* resources/food-process-lexicon.txt
	* resources/noun_adj_and_spelling_variants
	* corpus/&corpus;/batch/&batch;/yatea/candidates.xml
	* /projet/mig/save/textemig/projet-work/resources/YaTeA/config-habitats
	* /projet/mig/save/textemig/projet-work/resources/YaTeA/locale
	* /projet/mig/save/textemig/projet-work/biotopes/plans/yatea.rc
	* /projet/mig/work/textemig/biotopes/software/yatea-lib
	* &ontobiotope;_words.txt
 
* taxa.plan
	* /projet/mig/save/textemig/projet-work/resources/ncbi-taxonomy/taxa+id_full.txt
	* /projet/mig/save/textemig/projet-work/resources/ncbi-taxonomy/taxid_microorganisms.txt

* syntax.plan
	* /projet/mig/save/textemig/projet-work/software/install/CCG/bin/pos
	* /projet/mig/save/textemig/projet-work/software/install/CCG/models/pos_bio-1.00
	* /projet/mig/save/textemig/projet-work/software/install/CCG/bin/parser
	* /projet/mig/save/textemig/projet-work/software/install/CCG/models/parser
	* /projet/mig/save/textemig/projet-work/software/install/CCG/models/super_bio-1.00

* strains-2.plan

* pos-tag-lemma-postprocessing.plan

* tomap-habitats.plan
	* resources/stopwords_EN.ttg
	* resources/graylist_extended.heads
	* &ontobiotope;-Habitat.tomap
	* &ontobiotope;-Habitat.obo
	* corpus/&corpus;/batch/&batch;/yatea/candidates.xml
	* corpus/&corpus;/food-pmids.txt
	* resources/blacklist.txt
	* resources/NCBI_taxa_ontobiotope.txt

* tomap-microbial-phenotypes.plan
	* corpus/&corpus;/batch/&batch;/yatea/candidates.xml
	* resources/stopwords_EN.ttg
	* &ontobiotope;-Phenotype.tomap
	* &ontobiotope;-Phenotype.obo
	* corpus/&corpus;/batch/&batch;/yatea/candidates.xml
	* corpus/&corpus;/batch/&batch;/yatea-var/candidates.xml
	* words_prepro/default/xml/candidates_pp.xml
	* resources/blacklisted-phenotype-heads.txt
	* &ontobiotope;-Phenotype.obo

* use-extraction.plan
	* &ontobiotope-use;.obo

* anaphora.plan
	* resources/anaphoraLexicon.txt
	* resources/biAnaphoraLexicon.txt

* strains-1.plan

 * segmentation.plan
	* res://fr/inra/maiage/bibliome/alvisnlp/bibliomefactory/resources/latin_multi-word-expressions.txt
	

* tag-food-derivative.plan

* tag-food-process.plan

###
	
