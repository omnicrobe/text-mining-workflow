## config file
configfile: 'config/config.yaml'

## all
rule all:
	input:
		cirm='corpora/cirm/test.txt',
		cirm_yeast='corpora/cirm/test-yeast.txt',
		genbank='corpora/genbank/test-3.2.txt',
		dsmz='corpora/dsmz/test-3.3.txt',
		microbes_habitat='share/Florilege/2019-12-12/PubMed-Habitat-2019-12-12.txt',
		microbes_phenotype='share/Florilege/2019-12-12/PubMed-Phenotype-2019-12-12.txt',
		index_folder='corpora/pubmed/index',
		expander_folder='corpora/pubmed/expander'
		#onto_habitat_json='share/BioNLP-OST+EnovFood-Habitat.json',
		#onto_phenotype_json='share/BioNLP-OST+EnovFood-Phenotype.json',
		#onto_use_json='share/BioNLP-OST+EnovFood-Use.json'


rule preprocess_ontology:
	input:
		ontobiotope='share/BioNLP-OST+EnovFood.obo',
		use_onto='share/BioNLP-OST+EnovFood-Use.obo',
		names='share/extended-microorganisms-taxonomy/names.dmp'
	output:
		'share/BioNLP-OST+EnovFood-Habitat.json',
		'share/BioNLP-OST+EnovFood-Phenotype.json',
		'share/BioNLP-OST+EnovFood-Use.json',
		'share/BioNLP-OST+EnovFood-Habitat.tomap',
		'share/BioNLP-OST+EnovFood-Phenotype.tomap',
		#'share/BioNLP-OST+EnovFood-Use.tomap',
		'share/food-process-lexicon.txt',
		'share/NCBI_taxa_ontobiotope.txt',
		'share/BioNLP-OST+EnovFood-Habitat.obo',
		'share/BioNLP-OST+EnovFood-Phenotype.obo',
		'share/BioNLP-OST+EnovFood-Phenotype.paths',
		'share/BioNLP-OST+EnovFood-Habitat.paths',
		'share/BioNLP-OST+EnovFood-Use.paths'
	shell: """snakemake --verbose \
	    --printshellcmds \
	    --use-singularity \
	    --use-conda \
	    --reason \
	    --jobs 4  \
	    --snakefile preprocess-ontology.snakefile \
	    --profile config/profile \
	    all
	    """



rule process_cirm_corpus:
	input:
		cirm_data='corpora/cirm/2019-07-05/extraction_3-fv.csv',
		habitat_tomap='share/BioNLP-OST+EnovFood-Habitat.tomap',
		phenotype_tomap='share/BioNLP-OST+EnovFood-Phenotype.tomap',
		habitat_paths='share/BioNLP-OST+EnovFood-Habitat.paths', 
		phenotype_paths='share/BioNLP-OST+EnovFood-Phenotype.paths',
	output:
		'corpora/cirm/test.txt',
		'corpora/cirm/test-yeast.txt'
	shell: """snakemake --verbose \
	    --printshellcmds \
	    --use-singularity \
	    --use-conda \
	    --reason \
	    --jobs 4  \
	    --snakefile process_CIRM_corpus.snakefile \
	    --profile config/profile \
		all
	    """

rule process_genbank_corpus:
	input:
		genbank_data='corpora/genbank/req1_sup800_bacteria-descriptors.csv',
		habitat_tomap='share/BioNLP-OST+EnovFood-Habitat.tomap',
		phenotype_tomap='share/BioNLP-OST+EnovFood-Phenotype.tomap',
		habitat_paths='share/BioNLP-OST+EnovFood-Habitat.paths', 
		phenotype_paths='share/BioNLP-OST+EnovFood-Phenotype.paths',
		use_paths='share/BioNLP-OST+EnovFood-Use.paths'
	output:
		'corpora/genbank/test-3.2.txt'
	shell: """snakemake --verbose \
	    --printshellcmds \
	    --use-singularity \
	    --use-conda \
	    --reason \
	    --jobs 4  \
	    --snakefile process_GenBank_corpus.snakefile \
	    --profile config/profile \
		all
	    """

rule process_dsmz_corpus:
	input:
		dsmz_data1='corpora/dsmz/dsmz-data/category=from_ncbi_taxonomy-key=taxid.tsv',
		dsmz_data2='corpora/dsmz/dsmz-data/category=origin-key=sample_type.tsv',
		habitat_tomap='share/BioNLP-OST+EnovFood-Habitat.tomap',
		phenotype_tomap='share/BioNLP-OST+EnovFood-Phenotype.tomap',
		habitat_paths='share/BioNLP-OST+EnovFood-Habitat.paths', 
		phenotype_paths='share/BioNLP-OST+EnovFood-Phenotype.paths',
		use_paths='share/BioNLP-OST+EnovFood-Use.paths'
	output:
		'corpora/dsmz/test-3.3.txt'
	shell: """snakemake --verbose \
	    --printshellcmds \
	    --use-singularity \
	    --use-conda \
	    --reason \
	    --jobs 4  \
	    --snakefile process_DSMZ_corpus.snakefile \
	    --profile config/profile \
		all
	    """

## document batches
BATCHES, = glob_wildcards("corpora/pubmed/batches/{id}/batch.xml")

rule process_pubmed_corpus:
	input:
		habitat_tomap='share/BioNLP-OST+EnovFood-Habitat.tomap',
		phenotype_tomap='share/BioNLP-OST+EnovFood-Phenotype.tomap',
		habitat_paths='share/BioNLP-OST+EnovFood-Habitat.paths', 
		phenotype_paths='share/BioNLP-OST+EnovFood-Phenotype.paths',
		use_paths='share/BioNLP-OST+EnovFood-Use.paths'
	output:
		expander_folder=directory("corpora/pubmed/expander"),
		index_folder=directory("corpora/pubmed/index"),
		florilege_Habitat_result="share/2019-12-12/PubMed-Habitat-2019-12-12.txt",
		florilege_Phenotype_result="share/2019-12-12/PubMed-Phenotype-2019-12-12.txt"
	shell: """snakemake --verbose \
	    --printshellcmds \
	    --use-singularity \
	    --use-conda \
	    --reason \
	    --jobs 80  \
	    --snakefile process_PubMed_corpus.snakefile \
	    --profile config/profile \
		all
	    """
