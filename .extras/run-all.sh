#!/bin/bash

snakemake --verbose --printshellcmds --use-singularity --nolock --reason --latency-wait 30  --jobs 100 \
--snakefile all.snakefile all \
&& \
snakemake --verbose --printshellcmds --use-singularity --nolock --reason --latency-wait 30  --jobs 100 \
--snakefile all.snakefile all \
--report report.html
