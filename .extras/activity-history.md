## trace 

* singularity must me available in a way that permits to run snakemake pipelines without re-creating the image
* must check the process used to produce the data of Florilege (ckeck here projet-work/biotopes/Florilege)
* must analyse the manual process and derive the model of the snakemake workflow that generalize the manual process
* must consider the large number of files and big files, where and how to maintain them
