#!/bin/bash

cd softwares/ && \
singularity pull --docker-login alvisnlp.sif oras:registry.forgemia.inra.fr/migale/tm-tools-packages/sif/alvisnlp:latest && \
git clone https://github.com/Bibliome/obo-utils.git && \
mkdir -p ../alvisir-install && \
git clone https://github.com/Bibliome/alvisir.git && \
cd alvisir/ && \
mvn clean package && \
./install.sh ../alvisir-install &&\
cd .. && \
rm -rf alvisir && \
cd .. && \
conda env create -f softwares/envs/snakemake-5.13.0-env.yaml