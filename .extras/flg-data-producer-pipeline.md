# Produce the text mining data of Florilege

## Ontology preprocessing (required if new ontology version) ####

./preprocess_ontobiotope.sh BioNLP-OST+EnovFood

## Generate concept paths (for SD):

```
/projet/mig/work/textemig/software/install/alvisnlp/bin/alvisnlp \
-cleanTmp -verbose \
-alias input resources/BioNLP-OST+EnovFood-Habitat.obo \
-alias output \"BioNLP-OST+EnovFood-Habitat.paths\" 
-outputDir Florilege/2019-12-12/ \
plans/get_onto_paths.plan
```

```
/projet/mig/work/textemig/software/install/alvisnlp/bin/alvisnlp \
-cleanTmp -verbose \
-alias input resources/BioNLP-OST+EnovFood-Phenotype.obo \
-alias output \"BioNLP-OST+EnovFood-Phenotype.paths\" \
-outputDir Florilege/2019-12-12/ \
plans/get_onto_paths.plan
```

```
/projet/mig/work/textemig/software/install/alvisnlp/bin/alvisnlp \
-cleanTmp -verbose \
-alias input resources/Use_V2.obo \
-alias output \"Use_V2.paths\" \
-outputDir Florilege/2019-12-12/ \
plans/get_onto_paths.plan
```


## Databases #### 

* Processing CIRM data:

```
./Florilege/scripts/process-cirm-data.sh \
-i Florilege/DB/CIRM/2019-07-05/extraction_3-fv.csv \
-o CIRM-Habitats-2019-12-12.txt \
-d Florilege/test/cirm/ \
-v BioNLP-OST+EnovFood
```

* Processing GenBank data:

```
./Florilege/scripts/process-genbank-data.sh \
-i Florilege/DB/GenBank/req1_sup800_bacteria-descriptors.csv \
-o GenBank-Habitats-2019-12-12.txt  \
-d Florilege/test/genbank \
-v BioNLP-OST+EnovFood
```

* Processing DSMZ data:

```
./Florilege/scripts/process-dsmz-data.sh \
-t Florilege/DB/dsmz/dsmz-data/category=from_ncbi_taxonomy-key=taxid.tsv \
-h Florilege/DB/dsmz/dsmz-data/category=origin-key=sample_type.tsv \
-d Florilege/test/dsmz/ \
-o DSMZ-Habitats-2019-12-12.txt \
-v BioNLP-OST+EnovFood
```


## Text-mining workflow on PubMed #### 

* If new ontology version :
	* Open the Makefile and update the ontology version variable

* Running the pipeline:
	* ```make corpus/microbes-2019/entities.qsync```
	* ```nohup make corpus/microbes-2019/entities.log & ```

* Merging the results:
	* ```make corpus/microbes-2019/relations.txt```
	* ```make corpus/microbes-2019/phenotype-relations.txt``` 
	* ```make corpus/microbes-2019/uses-relations.txt```
	* ```make corpus/microbes-2019/microorganisms.txt``` 
	* ```make corpus/microbes-2019/habitats.txt```
	* ```make corpus/microbes-2019/phenotypes.txt```
	* ```make corpus/microbes-2019/uses.txt```

* Creating the index and expander for AlvisIR:
	* ```make corpus/microbes-2019/index```
	* ```make corpus/microbes-2019/expander```

* Formatting the results for integration in Florilege:

	* Relations

	```	
	python3 Florilege/scripts/format-pubmed-results.py \
	--pubmed-results corpus/microbes-2019/relations.txt \
	> Florilege/2019-12-12/PubMed-Habitat-2019-12-12.txt
	```
	
	* Phenotypes
	
	```
	python3 Florilege/scripts/format-pubmed-results.py \
	--pubmed-results corpus/microbes-2019/phenotype-relations.txt \
	> Florilege/2019-12-12/PubMed-Phenotype-2019-12-12.txt
	```

	* Phenotypes-Relations
	
	```
	python format-pubmed-results.py 
	--pubmed-results ../../corpus/microbes-2019/phenotype-relations.txt \
	>../test/res.txt
	```
