import os
## config file
configfile: "config/config.yaml"


''' 
all
'''
rule all:
    input:
        config["MATCH_RESULT_FOLDER"] + "/habitats.rankdiff.txt",
        config["MATCH_RESULT_FOLDER"] + "/microorganisms.rankdiff.txt",
        config["MATCH_RESULT_FOLDER"] + "/phenotypes.rankdiff.txt",
        config["MATCH_RESULT_FOLDER"] + "/uses.rankdiff.txt"
		


'''
rank
'''
rule rank_old_results:
    input:
        entities=config["OLD_RESULT_FOLDER"] + "/{entity}.full.txt"
    output:
        rank=config["OLD_RESULT_FOLDER"] + "/{entity}.rank.txt"
    shell:"""
        python softwares/scripts/rank.py 6 7 <{input.entities} >{output.rank}
        """

'''
rank
'''
rule rank_new_results:
    input:
        entities=config["NEW_RESULT_FOLDER"] + "/{entity}.full.txt"
    output:
        rank=config["NEW_RESULT_FOLDER"] + "/{entity}.rank.txt"
    shell:"""
        python softwares/scripts/rank.py 6 7 <{input.entities} >{output.rank}
        """


'''
compare
'''
rule compare:
    input:
        old=config["OLD_RESULT_FOLDER"] + "/{entity}.rank.txt",
        new=config["NEW_RESULT_FOLDER"] + "/{entity}.rank.txt"
    output:
        diff=config["MATCH_RESULT_FOLDER"] + "/{entity}.rankdiff.txt"
    shell:"""
        python softwares/scripts/compare-ranks.py {input.old} {input.new} >{output.diff}
        """
