## local rule
# localrules: all, concat_results

## config file
configfile: "config/config.yaml"

'''
## variables, check values into config file
_ontobiotope = config['ONTOBIOTOPE']
_names = config['NCBI_TAXO_NAMES']
_pubmed_batches_home = config['EPMC_BATCHES_HOME']
_pubmed_xslt_file = config['EPMC_XSLT_FILE']
_ncbi_taxo_microorganisms = config['NCBI_TAXO_MICROORGANISMS']
_ncbi_taxo_id = config['NCBI_TAXO_ID']
_ncbi_taxo_and_id_microorganisms = config['NCBI_TAXO_AND_ID_MICROORGANISMS']
'''


## document batches
DBATCHES, FILES = glob_wildcards(config["EPMC_BATCHES_HOME"] + "/{id}/PMC{file}.xml")
BATCHES = list(set(DBATCHES))

## list of the results
RESULTS = ["relations", "phenotype-relations", "uses-relations", "microorganisms", "habitats", "phenotypes", "uses"]


'''
all
'''
rule all:
    input:
        relations=expand(config["EPMC_BATCHES_HOME"] + "/{B}/relations.txt", B=BATCHES),
        phenotypeRelations=expand(config["EPMC_BATCHES_HOME"] + "/{B}/phenotype-relations.txt", B=BATCHES),
        usesRelations=expand(config["EPMC_BATCHES_HOME"] + "/{B}/uses-relations.txt", B=BATCHES),
        microorganisms=expand(config["EPMC_BATCHES_HOME"] + "/{B}/microorganisms.txt", B=BATCHES),
        habitats=expand(config["EPMC_BATCHES_HOME"] + "/{B}/habitats.txt", B=BATCHES),
        phenotypes=expand(config["EPMC_BATCHES_HOME"] + "/{B}/phenotypes.txt", B=BATCHES),
        uses=expand(config["EPMC_BATCHES_HOME"] + "/{B}/uses.txt", B=BATCHES),
        index=expand(config["EPMC_BATCHES_HOME"] + "/{B}/index", B=BATCHES),
        index_folder=config["EPMC_CORPUS_HOME"] + "/index",
        expander_folder="corpora/epmc/expander",
        result=expand("corpora/epmc/{R}.full.txt", R=RESULTS),
        florilege_Habitat_result="corpora/florilege/epmc/EPMC-Habitat.txt",
        florilege_Phenotype_result="corpora/florilege/epmc/EPMC-Phenotype.txt",
        florilege_Use_result="corpora/florilege/epmc/EPMC-Use.txt"


'''
Extract entities in different corpus 
batches using the alvisnlp plan (omnicrobe_main.plan)
'''
rule run_epmc_main:
    input:
        file = config['EPMC_BATCHES_HOME'] + "/{B}/",
        xslt = config['EPMC_XSLT_FILE']
    output:
        results=expand(config["EPMC_BATCHES_HOME"] + "/{{B}}/{R}.txt", R=RESULTS),
        index=directory(config["EPMC_BATCHES_HOME"] + "/{B}/index"),
        yatea_candidates=config["EPMC_BATCHES_HOME"] + "/{B}/yatea/candidates.xml",
        yatea_var_candidates=config["EPMC_BATCHES_HOME"] + "/{B}/yatea-var/candidates.xml"
    params:
        batch="{B}",
        corpus='empc',
        onto_habitat='share/BioNLP-OST+EnovFood-Habitat.obo',
        tomap_habitat='share/BioNLP-OST+EnovFood-Habitat.tomap',
        onto_pheno='share/BioNLP-OST+EnovFood-Phenotype.obo',
        tomap_pheno='share/BioNLP-OST+EnovFood-Phenotype.tomap',
        graylist='share/graylist_extended.heads',
        emptywords='share/stopwords_EN.ttg',
        onto_use='share/BioNLP-OST+EnovFood-Use.obo',
        plan='plans/omnicrobe_main.plan',
        dir=config["EPMC_BATCHES_HOME"] + "/{B}/",
        taxid_microorganisms=config['NCBI_TAXO_MICROORGANISMS'],
        taxa_id_full=config['NCBI_TAXO_ID'],
        dummy= config["EPMC_BATCHES_HOME"] + '/{B}/bionlp-st',
        log="alvisnlp.log"
    singularity:config["SINGULARITY_IMG"]
    shell:"""
        rm -f {output.yatea_candidates} {output.yatea_var_candidates} && mkdir -p {params.dummy} && alvisnlp -J-XX:+UseSerialGC -J-Xmx30g -cleanTmp -verbose \
        -log {params.log} \
        -alias format pubmed \
        -alias input {input.file} \
        -alias input-xslt {input.xslt} \
        -alias batch batch={params.batch} \
        -outputDir {params.dir} \
        -alias ontobiotope-habitat {params.onto_habitat} \
        -xalias '<ontobiotope-tomap-habitat empty-words="{params.emptywords}" graylist="{params.graylist}" whole-proxy-distance="false">{params.tomap_habitat}</ontobiotope-tomap-habitat>' \
        -alias ontobiotope-phenotypes {params.onto_pheno} \
        -xalias '<ontobiotope-tomap-phenotypes empty-words="{params.emptywords}" whole-proxy-distance="false">{params.tomap_pheno}</ontobiotope-tomap-phenotypes>' \
        -alias ontobiotope-use {params.onto_use} \
        -alias taxid_microorganisms {params.taxid_microorganisms} \
        -alias taxa+id_full {params.taxa_id_full} \
        {params.plan}
        """


'''
select results to concat

rule selectToConcat:
    input:
        results=expand(config["EPMC_BATCHES_HOME"] + "/{{B}}/{R}.txt", R=RESULTS),
        index=config["EPMC_BATCHES_HOME"] + "/{B}/index"
    output:
        results=config["EPMC_CORPUS_HOME"] + "/{B}/{R}.txt"
'''


'''
concat the different results
for 
* relations 
* phenotype-relations 
* uses-relations 
* microorganisms 
* habitats 
* phenotypes 
* uses
/!\ bash arguments too long if you use cat
'''
rule concat_results:
    input: 
        expand(config["EPMC_BATCHES_HOME"] + "/{B}/{{R}}.txt", B=BATCHES)
    output:
        result="corpora/epmc/{R}.full.txt"
    run:
        with open(output.result, 'w') as out:
            for fname in input:
                with open(fname) as infile:
                    out.write(infile.read())

'''
select files to be formated
'''
rule select:
    input:
        files=expand("corpora/epmc/{R}.full.txt", R=RESULTS)
    output:
        relation="corpora/epmc/relations.full.txt",
                phenotype_relations="corpora/epmc/phenotype-relations.full.txt",
                use_relations="corpora/epmc/uses-relations.full.txt"
    shell:"""
        """
            


'''
merge indexes from the batches
'''
rule merge_epmc_index:
    input:
        index=expand(config["EPMC_BATCHES_HOME"] +"/{B}/index", B=BATCHES)
    output:
        index_folder=directory(config["EPMC_CORPUS_HOME"] + "/index")
    params:
        alvisir=config["ALVISIR_HOME"]
    shell: """
        java -cp {params.alvisir}/lib/lucene-core-3.6.1.jar:{params.alvisir}/lib/lucene-misc-3.6.1.jar \
        org.apache.lucene.misc.IndexMergeTool \
        {output.index_folder} {input.index}
        """



'''
create the expander
'''
rule create_epmc_expander:
    input:
        expander="share/expander.xml",
        taxa_id_microorganisms=config['NCBI_TAXO_AND_ID_MICROORGANISMS'],
                onto_habitat="share/BioNLP-OST+EnovFood-Habitat.obo",
        onto_phenotype="share/BioNLP-OST+EnovFood-Phenotype.obo",
        onto_use="share/BioNLP-OST+EnovFood-Use.obo"
    output:
        expander_folder=directory( config["EPMC_CORPUS_HOME"] + "/expander")
    params:
        alvisir=config["ALVISIR_HOME"]
    shell:"""
        {params.alvisir}/bin/alvisir-index-expander {output.expander_folder} {input.expander}
          """


'''
formatting the extracted *relations* for 
integration in Florilege
'''
rule format_epmc_relations:
    input:
        file="corpora/epmc/relations.full.txt"
    output:
        florilege_result="corpora/florilege/epmc/EPMC-Habitat.txt"
    conda: 'softwares/envs/python3_env.yaml'
    shell:"""
        python softwares/scripts/format-pubmed-results.py \
        --pubmed-results {input.file} \
        > {output.florilege_result}
          """


'''
formatting the extracted *phenotype relations* for 
integration in Florilege
'''
rule format_epmc_phenotype_relations:
    input:
        file="corpora/epmc/phenotype-relations.full.txt"
    output:
        florilege_result="corpora/florilege/epmc/EPMC-Phenotype.txt"
    conda: 'softwares/envs/python3_env.yaml'
    shell:"""
        python softwares/scripts/format-pubmed-results.py \
        --pubmed-results {input.file} \
        > {output.florilege_result}
          """

'''
formatting the extracted *use relations* for 
integration in Florilege
'''
rule format_epmc_use_relations:
    input:
        file="corpora/epmc/uses-relations.full.txt"
    output:
        florilege_result="corpora/florilege/epmc/EPMC-Use.txt"
    conda: 'softwares/envs/python3_env.yaml'
    shell:"""
        python softwares/scripts/format-pubmed-results.py \
        --pubmed-results {input.file} \
        > {output.florilege_result}
          """